<%-- 
    Document   : header.jsp
    Created on : Aug 7, 2017, 1:35:41 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="${context}"><strong>Harvester</strong> <small>&mdash; National Virtual Library of India</small></a>
        <a class="btn btn-sm btn-primary float-right" href="${context}/dashboard">Dashboard</a>
    </div>
</nav>
<!--<br>-->