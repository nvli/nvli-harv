<%-- 
    Document   : head
    Created on : Aug 7, 2017, 1:45:03 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" type="image/png" href="${context}/resources/images/harvester-icon.png"/>
<link rel="stylesheet" href="${context}/comp/tether/dist/css/tether.min.css"/>
<link rel="stylesheet" href="${context}/comp/bootstrap/dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="${context}/comp/font-awesome/css/font-awesome.min.css"/>
<!--<link rel="stylesheet" href="${context}/comp/semantic/dist/semantic.min.css"/>-->
<link rel="stylesheet" href="${context}/resources/css/site.css"/>
<link rel="stylesheet" href="${context}/comp/toastr/toastr.min.css"/>
<script src="${context}/comp/jquery/dist/jquery.min.js"></script>
<script src="${context}/comp/tether/dist/js/tether.min.js"></script>
<script src="${context}/comp/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${context}/comp/toastr/toastr.min.js"></script>
<!--<script src="${context}/comp/semantic/dist/semantic.min.js"></script>-->
<script src="${context}/resources/js/underscore.min.js"></script>
<!--<script src="${context}/comp/angular/angular.min.js"></script>-->

<script>
    var HARAPP = HARAPP || {};
    HARAPP.contextURL = "${context}";
</script>