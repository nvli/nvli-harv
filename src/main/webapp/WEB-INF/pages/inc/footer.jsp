<%-- 
    Document   : footer.jsp
    Created on : Aug 7, 2017, 1:35:51 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<footer class="footer">
    <section class="container">
        <p class="footer">All rights reserved <i class="fa fa-copyright"></i> 2017, <a href="${context}">Harvester</a> &mdash; National Virtual Library of India</p>
    </section>
</footer>