<%--
    Document   : example
    Created on : Apr 6, 2016, 2:32:53 PM
    Author     : Ankita Dhongde
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="inc/commmon-head.jsp" %>
        <title>Harvester &mdash; National Virtual Library of India</title>
        <link href="toastr.css" rel="stylesheet"/>
        <script src="toastr.js"></script>
        <style>
            .right-space-20{
                margin-right: 20px;
            }
            body{
                background: #f5f5f5;
            }
        </style>
    </head>
    <body>
        <%@include file="inc/header.jsp" %>
        <section class="container jumbotron">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h5 style="color:#555;"><strong>Add Repository</strong></h5><br>
                    <form id="frm-harvest-url" class="form-inline">
                        <div class="form-group">
                            <label class="right-space-20">Base URL</label>
                            <input type="text" id="baseUrlInput" name="baseURL"  class="form-control right-space-20" style="width:500px;" required="required">
                            <input type="submit" value="Go" id="go" class="btn btn-primary right-space-20">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="container jumbotron" id="frm-create-repo-container" style="display: none;background: #ffeacb;">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h5><strong>Repository Details</strong></h5><br>
                    <form id="frm-create-repo" method="POST">
                        <input type="hidden" id="baseUrl" name="baseURL" class="form-control"/>
                        <div class="form-group">
                            <label for="adminEmail">Admin Email</label>
                            <input type="text" id="adminEmail" name="email" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" name="description" rows="5" class="form-control" ></textarea>
                        </div>
                        <div class="form-group">
                            <label for="repositoryName">Repository Name</label>
                            <input type="text" id="repositoryName" name="repositoryName" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="repoUID">Resource Code</label>
                            <input type="text" id="repoUID" name="repoUID" rows="5" class="form-control" required/>
                            <p class="repouid help red"></p>
                        </div>
                        <div class="form-group">
                            <label for="metadataType">Supported Metadata Formats</label>
                            <ol id="metadataType">

                            </ol>
                        </div>
                        <!--<input type="hidden" id="granularityDate" name="granularityDate"/>-->
                        <div class="form-group">
                            <button class="btn btn-primary" id="createRepository">Create Repository</button>
                            <a href="#" class="btn btn-secondary btn-danger btn-cancel-form" >Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="container jumbotron">
            <div class="row">
                <div class="col-md-6">
                    <h6 style="color:#555;"><strong> <i class="fa fa-link"></i>  Example OAI-PMH links</strong></h6>
                    <br>
                    <div class="list-group">
                        <a class="list-group-item list-group-item-action" href="http://dspace.library.iitb.ac.in/oai/request?verb=Identify" target="_blank">http://dspace.library.iitb.ac.in/oai/request</a>
                        <a class="list-group-item list-group-item-action" href="http://export.arxiv.org/oai2?verb=Identify" target="_blank">http://export.arxiv.org/oai2</a>
                        <a class="list-group-item list-group-item-action" href="http://dyuthi.cusat.ac.in/oai/request?verb=Identify" target="_blank">http://dyuthi.cusat.ac.in/oai/request</a>
                        <a class="list-group-item list-group-item-action" href="http://etd.ncsi.iisc.ernet.in/dspace-oai/request?verb=Identify" target="_blank">http://etd.ncsi.iisc.ernet.in/dspace-oai/request</a>
                        <a class="list-group-item list-group-item-action" href="http://oar.icrisat.org/cgi/oai2?verb=Identify" target="_blank">http://oar.icrisat.org/cgi/oai2</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <h6><strong><i class="fa fa-link"></i> Quick Verbs on IITB-OAIPMH</strong></h6>
                    <br>
                    <div class="list-group">
                        <a class="list-group-item list-group-item-action" href="${pageContext.servletContext.contextPath}/identify?baseURL=http://dspace.library.iitb.ac.in/oai/request" id="identify">Identify</a>
                        <a class="list-group-item list-group-item-action" href="${pageContext.servletContext.contextPath}/listSets?baseURL=http://dspace.library.iitb.ac.in/oai/request" id="listSets">ListSets</a>
                        <a class="list-group-item list-group-item-action" href="${pageContext.servletContext.contextPath}/listMetadataFormats?baseURL=http://dspace.library.iitb.ac.in/oai/request" id="listMetadataFormats">ListMetadataFormats</a>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="modal-start-harvest">
            <div class="modal-dialog success" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Repository Created Successfully</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="repo-details">
                            <p>Repository <span id="m-repo-name" style="font-weight: bold;"></span> with REPO_ID <span id="m-repo-id" style="font-weight: bold;"></span> is created successfully</p>
                            <br>
                            <form name="frm-harvest-repo" method="get" action="${context}/OAIPMHrepository/harvesteRepository">
                                <input type="hidden" name="repoUID" id="input-repo-uid" value="">

                                <input type="submit" class="btn btn-primary"
                                       id="btn-harvest-now" data-repo-uid=""
                                       value="Harvest Repository Now" type="submit">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="inc/footer.jsp" %>
        <script type="text/template" id="tpl-metadata-type-format-check">
            <li>{{=metadataType}}</li>

        </script>

        <script type="text/template" id="tpl-alert">
            <div class="alert alert-{{=type}} alert-dismissible fade show" role="alert">
            {{=message}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
        </script>

        <script type="text/javascript" >
function handleData(data) {
    $("#go").hide();
    console.log(data);
    console.log(data.adminEmail[0]);
    $("#frm-create-repo-container").show();
    $("#baseUrl").val(data.baseURL);
    $("#adminEmail").val(data.adminEmail);
    $("#description").val(data.description);
    $("#repositoryName").val(data.repositoryName);

    $("#metadataType").empty();
    _.each(data.supportedMetadataTypes, function (value, key) {
        var tpl = _.template($("#tpl-metadata-type-format-check").html());
        var checkEl = tpl({metadataType: key, isActive: value});
        $("#metadataType").append(checkEl);
    });
}

function checkResourceCode() {
    if ($("#repoUID").val().length < 4) {

        $(".repouid.help").append("<div class='validation' style='color:red;'>Resource code length should  be minimum 4 </div>");
        return false;
    }

    $.ajax({
        url: "${context}/OAIPMHrepository/checkRepositoryIsExist?repoUID=" + $("#repoUID").val(),
        type: 'GET',
        success: function (response) {
            if (response === true) {
                console.log("Resource code is alredy exist");
                $(".repouid.help").append("<div class='validation' style='color:red;'>Resource Code is alredy available </div>");
            }
        }
    });
}


$(document).ready(function () {
    $("#go").click(function (e) {
        console.log("Validating");
        var baseURL = $("#baseUrlInput").val();
        if (!baseURL) {
            if ($("#baseUrlInput").parent().next(".validation").length === 0) {
                $("#baseUrlInput").parent().after("<div class='validation' style='color:red;margin:10px 0 20px 75px;'>Please enter URL</div>");
            }
            // prevent form from POST to server
            $("#baseUrlInput").focus();
            //e.preventDefault();
        } else {
            $("#baseUrlInput").parent().next(".validation").remove(); // remove it
            $.ajax({
                url: "${context}/OAIPMHrepository/checkBaseURL",
                type: 'GET',
                data: {
                    baseURL: baseURL
                },
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    console.log(data.updateStatus);
                    if (data.updateStatus != "suceess") {
                        $("#baseUrlInput").parent().after("<div class='validation' style='color:red;margin:10px 0 20px 75px;'>" + data.updateStatus + "</div>");
                        $('#baseUrlInput').focus();
                    } else {
                        console.log("inside else");

                        var data = $("#frm-harvest-url").serialize();
                        // ajax call
                        $.ajax({
                            url: "${context}/OAIPMHrepository/identifyBaseUrl",
                            data: data,
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
                                handleData(data);
                            }
                        });

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("something went wrong!!!");
                }
            });
            return false;
        }
    });

    $(".btn-cancel-form").click(function () {
        $("#frm-create-repo-container").hide();
        $("#go").show();
        document.getElementById("frm-harvest-url").reset();
    });

    $('#repoUID').on("blur", function () {
        $(".repouid.help").empty();
        checkResourceCode();
    });


    $("#frm-create-repo").submit(function (e) {
        e.preventDefault();

        var data = $("#frm-create-repo").serializeArray();
        console.log(data);
        var dd = new Object();
        dd["baseURL"] = $("#baseUrl").val();
        dd["email"] = $("#email").val();
        dd["description"] = $("#description").val();
        dd["repositoryName"] = $("#repositoryName").val();
        dd["checkboxMetadataType"] = new Array();
        $(".form-check-meta:checkbox[name=checkboxMetadataType]:checked").each(function () {
            dd.checkboxMetadataType.push($(this).val());
        });
        dd["repoUID"] = $("#repoUID").val();

        $.ajax({
            url: "${context}/OAIPMHrepository/addRepository",
            data: JSON.stringify(dd),
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            //                        contentType: 'application/json',
            success: function (response) {
                if (response.status === true) {
                    $("#m-repo-id").html(response.repoUID);
                    $("#m-repo-name").html(response.repositoryName);
                    $("#btn-harvest-now").attr("data-repo-uid", response.repoUID);
                    $("#input-repo-uid").val(response.repoUID);
                    $("#btn-harvest-now").bind("click", function (e) {
                        console.log("submitting form", e);
                        $("#frm-harvest-repo").submit();
                        $("#modal-start-harvest").modal("hide");
                        $("#frm-create-repo-container").hide();
                        $("#go").show();
                        document.getElementById("frm-harvest-url").reset();
                        toastr.success('Harvesting Started Sucessfully');

                    });
                    $("#modal-start-harvest").modal("show");
                } else {
                    toastr.error("Unable to save repository");
                }
            }
        });
    });
});
        </script>
    </body>
</html>
