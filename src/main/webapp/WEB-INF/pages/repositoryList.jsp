<%-- 
    Document   : example
    Created on : Apr 6, 2016, 2:32:53 PM
    Author     : Ankita Dhongde
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="inc/commmon-head.jsp" %>
        <title>Harvester &mdash; National Virtual Library of India</title>
        <style>
            .right-space-20{
                margin-right: 20px;
            }
        </style>
    </head>
    <body>
        <%@include file="inc/header.jsp" %>
        
        <section class="container jumbotron">
            <table class="table table-bordered table-hover table-striped">
                <thead >
                    <tr>
                        <th>Sr No.</th>
                        <th>RepoSitory Name</th>
                        <th>RepoSitory Code</th>
                        <th>Repository Url</th>
                        <th>Supported Metadata</th>
                        <th>Repository Staus</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <c:set var = "count" value="0" />
                    <c:forEach items="${RepoList}" var="repo" >
                    <tr>
                        <td>${count+1} </td>
                        <td>${repo.repoName}</td>
                        <td>${repo.repoUID}</td>
                        <td>${repo.repoBaseUrl}</td>
                        <td></td>
                        <td>${repo.repoStatusId.repoStatusName}</td>
                        <td><a></a></td>
                    </tr>
                    <c:set var = "count" value="${count+1}" />
                    </c:forEach>
                </tbody>
            </table>
        </section>


        <%@include file="inc/footer.jsp" %>
        
    </body>
</html>

