<%-- 
    Document   : 4xx
    Created on : 16 Jan, 2018, 4:48:54 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../inc/commmon-head.jsp" %>
        <title>Harvester &mdash; National Virtual Library of India</title>
    </head>
    <body>
        <%@include file="../inc/header.jsp" %>
        <section class="container jumbotron">
            <center>
                <h1 style="color: #444; font-weight: bold;">Oops! Something screwed up...</h1>
                <br>
                <p>${errorMsg}</p>
            </center>
        </section>
    </body>
</html>
