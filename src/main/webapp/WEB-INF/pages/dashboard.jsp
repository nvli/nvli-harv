<%-- 
    Document   : dashboard
    Created on : Aug 7, 2017, 1:34:43 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="inc/commmon-head.jsp" %>
        <!--<link rel="stylesheet" href="${context}/comp/semantic/dist/semantic.min.css"/>-->
        <!--<script src="${context}/comp/semantic/dist/semantic.min.js"></script>-->
        <title>
            Dashboard :: Harvester &Rightarrow; National Virtual Library of India
        </title>
    </head>
    <body>
        <%@include file="inc/header.jsp" %>
        <section class="container" style="border-right: 1px solid #efefef; border-left: 1px solid #efefef; padding: 30px; background: #f8f8ff;">
            <div class="row">
                <div class="col-md-12">
                    <div id="select-repo-container">
                        <div class="ui sub header">Select Repository</div>
                        <select id="select-repo" class="ui fluid search dropdown form-control">
                            <option value="">Select</option>
                            <c:forEach items="${repositories}" var="repo">
                                <option data-uid="${repo.repoUID}" value="${repo.repoUID}">${repo.repoUID} - ${repo.repoName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br>
                    <div id="repo-details-container"  style="display: none;">
                        <h4><b>Repository Details</b></h4><br>
                        <div id="repo-details" class="repo-details"></div><br>
                        <div class="row">
                            <div class="col-12"><h5><strong>Supported Metadata Types</strong></h5></div>
                        </div><br>
                        <div id="metadata-type-container" style="padding: 30px 30px 15px 30px; background: #efefef; border-radius: 4px;"></div>
                        <br>
                        <div id="action-btn-container">
                            <div id="action-btn-container">
                                <button class="btn btn-primary" data-repo-uid="" id="btn-harvest-repo" data-repo-action="harvest"><span class="fa fa-refresh"></span> Harvest Repository</button>&nbsp;
                                <button class="btn btn-outline-primary" data-repo-uid="" id="btn-edit-repo" data-repo-action="edit"><span class="fa fa-pencil-square-o"></span> Edit This Repository</button>&nbsp;
                                <button class="btn btn-outline-danger" data-repo-uid="" id="btn-delete-repo" data-repo-action="delete"><span class="fa fa-trash"></span> Delete This Repository</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%@include file="inc/footer.jsp" %>
        <script type="text/template" id="tpl-har-repo-details">
            <div class="row">
                <div class="col-2"><p><b>Repository Name</b></p></div>
                <div class="col-1">&Rightarrow;</div>
                <div class="col-9">{{=repoName}}</div>
            </div>
            <div class="row">
                <div class="col-2"><p><b>Repository UID</b></p></div>
                <div class="col-1">&Rightarrow;</div>
                <div class="col-9">{{=repoUID}}</div>
            </div>
            <div class="row">
                <div class="col-2"><p><b>Repository URL</b></p></div>
                <div class="col-1">&Rightarrow;</div>
                <div class="col-9">{{=repoBaseURL}}</div>
            </div>
            <div class="row">
                <div class="col-2"><p><b>Description</b></p></div>
                <div class="col-1">&Rightarrow;</div>
                <div class="col-9">{{=description}}</div>
            </div>
        </script>
        <script type="text/template" id="tpl-har-metadata-type">
            <div class="row">
                <div class="col-2"><span style="text-transform:uppercase;font-weight:bold;">{{=count}}. {{=metadataTypeName}}</span></div>
                <div class="col-7">{{=status}}</div>
                <div class="col-3">
                    {{ if(btnLabel === "show_progress"){ }}
                            <img src="${context}/resources/images/processing.gif" class="pull-right"/>
                    {{ } else { }}
                    <button class="btn btn-outline-{{=btnType}} btn-sm pull-right" metadata-type-name="{{=metadataTypeName}}" data-repo-uid="{{=repoId}}" id="{{=btnId}}">{{=btnLabel}}</button>
                    {{ } }}
                </div>
            </div>
            <hr>
        </script>
        <script src="${context}/resources/js/dashboard.js"></script>
    </body>
</html>
