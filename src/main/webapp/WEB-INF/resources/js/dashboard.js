/* global jQuery, _, HARAPP */

(function ($, _, APP) {
    function logHeading(msg) {
        console.info("%c" + msg, "color: #793939;font-weight:bold;font-size:20px;");
    }
    function logInfo(msg) {
        console.info("%c" + msg, "color: #5C9154;font-weight:bold;");
    }
    function printFancyLogs() {

        logHeading("Harvester — National Virtual Library of India");
        logInfo("\u2714 DOM Ready");

        if (_.size($('div'))) {
            logInfo("\u2714 jQuery Loaded");
            logInfo("\u2714 Underscore.js Loaded");
        }
    }

    function renderRepoDetails(repo_uid) {
        $.get(APP.contextURL + "/g/repo/details/" + repo_uid, function (data) {
            console.log(data);
            $("#repo-details").empty();

            var tpl = _.template($("#tpl-har-repo-details").html());
            var opts = {
                repoId: data.repoId,
                repoName: data.repoName,
                repoUID: data.repoUID,
                repoBaseURL: data.repoBaseURL,
                description: data.repoDesc
            };
            $("#repo-details").append(tpl(opts));
            $("#metadata-type-container").empty();
            _.each(data.metadataList, function (obj, i) {
                var tplMetadata = _.template($("#tpl-har-metadata-type").html());
                i++;
                var opts = {
                    count: i,
                    status: obj.statusDesc,
                    metadataTypeName: obj.metadataType,
                    btnType: "primary",
                    btnLabel: obj.nextAction,
                    repoId: data.repoId,

                    metadataId: data.metadataId,
                    btnId: obj.nextAction.replace(" ", "-").toLowerCase()
                };
                $("#metadata-type-container").append(tplMetadata(opts));

                $("#" + opts.btnId).click(function (e) {
                    console.log("insie");
                    $.ajax({
                        url: APP.contextURL + "/OAIPMHrepository/",
                        data: {repoUID: repoUID, metadataTypeName: opts.metadataTypeName},
                        type: 'POST',
                        success: function (data, textStatus, jqXHR) {

                        }

                    });
                });

            });
            $("#btn-harvest-repo, #btn-edit-repo, #btn-delete-repo").attr("data-repo-uid", data.repoUID);
            $("#btn-harvest-repo").click(function (e) {
                var repoUID = e.currentTarget.getAttribute("data-repo-uid");
                $.post(APP.contextURL + "/OAIPMHrepository/harvesteRepository",
                        {repoUID: repoUID},
                        function (response) {
                            console.log(response);
                        }
                );
            });

            $("#btn-delete-repo").click(function (e) {
                var repoUID = e.currentTarget.getAttribute("data-repo-uid");
                logInfo("Deleting repo..." + repoUID);
                $.get(APP.contextURL + "/OAIPMHrepository/repo/xd", {repoUID: repoUID},
                function (response) {
                    console.log(response);
                    if (response === true)
                        top.location.reload();
                });
            });

            $("#btn-edit-repo").click(function (e) {

            })

        });
    }

    $(document).ready(function () {
        printFancyLogs();

        $("#select-repo").change(function (e) {
            console.log(e.currentTarget.options);
            $("#repo-details-container").show();
            renderRepoDetails(e.currentTarget.options[e.currentTarget.options.selectedIndex].value);
        });
    });
}(jQuery, _, HARAPP));
