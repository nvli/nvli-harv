package in.gov.nvli.harvester.dto;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
public class NewRepoDTO {

    private String repoUID;
    private String repositoryName;
    private String email;
    private String baseURL;
    private String description;
    private String[] checkboxMetadataType;

    public String getRepoUID() {
        return repoUID;
    }

    public void setRepoUID(String repoUID) {
        this.repoUID = repoUID;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getCheckboxMetadataType() {
        return checkboxMetadataType;
    }

    public void setCheckboxMetadataType(String[] checkboxMetadataType) {
        this.checkboxMetadataType = checkboxMetadataType;
    }

}
