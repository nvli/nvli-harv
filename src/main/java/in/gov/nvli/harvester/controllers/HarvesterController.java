package in.gov.nvli.harvester.controllers;

import in.gov.nvli.harvester.customised.HarRepoCustomised;
import in.gov.nvli.harvester.dao.HarRepoStatusDao;
import in.gov.nvli.harvester.services.HarvesterService;
import in.gov.nvli.harvester.services.RepositoryService;
import java.net.URI;
import java.net.URISyntaxException;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author vootla
 */
@Controller
@EnableAsync
public class HarvesterController {

    @Autowired
    HarvesterService harvesterService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HarRepoStatusDao harRepoStatusDao;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GetRecordController.class);

    @RequestMapping("/")
    public String indexMethod() {
        return "index";
    }

    @RequestMapping("/harvester")
    public String harvestRepository(@RequestParam("baseURL") String baseURL) {
        harvesterService.harvestRepository(baseURL);

        return "index";
    }

    @RequestMapping("/harvestall")
    public String harvestAll() {
        harvesterService.harvestAllRepositories();
        return "index";
    }

    @RequestMapping("/harvester_incremental")
    public String harvestRepositoryIncremental(@RequestParam("baseURL") String baseURL) {
        harvesterService.harvestRepositoryIncremental(baseURL);
        return "index";
    }

    @RequestMapping("/harvest_all_incremental")
    public String harvestAllRepositoriesIncremental() {
        harvesterService.harvestAllRepositoriesIncremental();
        return "index";
    }

    @RequestMapping("/test")
    public void test() throws URISyntaxException {
        URI webServiceURL = new URI("http://localhost:8080/nvliHarvester/rest/repositories");//"http://nvli-vm.pune.cdac.in:8080/harvester/rest/repositories"
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.httpClient));
        HarRepoCustomised harRepoCustomised = new HarRepoCustomised();
        harRepoCustomised.setRepoBaseUrl("http://export.arxiv.org/oai2");
        harRepoCustomised.setRepoStatusId((short) 1);
        harRepoCustomised.setRepoUID("1");
        ResponseEntity<HarRepoCustomised> responseObj = restTemplate.postForEntity(webServiceURL, harRepoCustomised, HarRepoCustomised.class);
        LOGGER.info("Has done " + responseObj.getStatusCode());
    }

    @RequestMapping("/dashboard")
    public ModelAndView dashboard() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("dashboard");
        modelAndView.addObject("repositories", repositoryService.getAllRepositories());
        modelAndView.addObject("repoStatusList", harRepoStatusDao.list());
        return modelAndView;
    }

    @RequestMapping(value = "/g/repo/details/{repo_uid}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public JSONObject repoDetails(@PathVariable(value = "repo_uid") String repo_uid) {
        return repositoryService.getRepoDetails(repo_uid);
    }
}
