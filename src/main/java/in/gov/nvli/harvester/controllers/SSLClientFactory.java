/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.controllers;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.*;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public abstract class SSLClientFactory {

    public enum HttpClientType {

        httpClient
    }

    public static synchronized ClientHttpRequestFactory getClientHttpRequestFactory(HttpClientType httpClientType) {

        ClientHttpRequestFactory requestFactory = null;
        SSLContext sslContext = SSLClientFactory.getSSlContext();

        if (null == sslContext) {
            return requestFactory;
        }

        if (httpClientType == HttpClientType.httpClient) {
            HttpClientBuilder closeableClientBuilder = HttpClientBuilder.create();

            //Add the SSLContext and trustmanager
            closeableClientBuilder.setSSLContext(getSSlContext());
            //add the hostname verifier
            closeableClientBuilder.setSSLHostnameVerifier(gethostnameVerifier());

            requestFactory = new HttpComponentsClientHttpRequestFactory(closeableClientBuilder.build());
        }
        return requestFactory;
    }

    public static SSLContext getSSlContext() {
        final TrustManager[] trustAllCerts = new TrustManager[]{getTrustManager()};
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

        } catch (NoSuchAlgorithmException | KeyManagementException e) {
        }
        return sslContext;

    }

    public static X509TrustManager getTrustManager() {
        return new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                X509Certificate[] cArrr = new X509Certificate[0];
                return cArrr;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                // TODO Auto-generated method stub
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                // TODO Auto-generated method stub
            }
        };
    }

    private static HostnameVerifier gethostnameVerifier() {

        return new HostnameVerifier() {

            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };
    }
}
