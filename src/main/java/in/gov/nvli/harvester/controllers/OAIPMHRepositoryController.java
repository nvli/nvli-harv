package in.gov.nvli.harvester.controllers;

import in.gov.nvli.harvester.beans.HarMetadataType;
import in.gov.nvli.harvester.beans.HarMetadataTypeRepository;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.beans.HarRepoMetadata;
import in.gov.nvli.harvester.beans.HarRepoStatus;
import in.gov.nvli.harvester.constants.CommonConstants;
import in.gov.nvli.harvester.constants.HarvesterLogConstants;
import in.gov.nvli.harvester.custom.exception.OAIPMHerrorTypeException;
import in.gov.nvli.harvester.custom.harvester_enum.HarRecordMetadataTypeEnum;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import in.gov.nvli.harvester.custom.harvester_enum.RepoStatusEnum;
import in.gov.nvli.harvester.customised.IdentifyTypeCustomised;
import in.gov.nvli.harvester.dao.HarMetadataTypeDao;
import in.gov.nvli.harvester.dao.HarRepoMetadataDao;
import in.gov.nvli.harvester.dao.RepositoryDao;
import in.gov.nvli.harvester.dto.NewRepoDTO;
import in.gov.nvli.harvester.oaipmhbeans.IdentifyType;
import in.gov.nvli.harvester.oaipmhbeans.MetadataFormatType;
import in.gov.nvli.harvester.oaipmhbeans.VerbType;
import in.gov.nvli.harvester.service.OAIPMHRepositoryService;
import in.gov.nvli.harvester.service.RepositoryHarvestService;
import in.gov.nvli.harvester.services.IdentifyService;
import in.gov.nvli.harvester.services.ListMetadataFormatsService;
import in.gov.nvli.harvester.services.ListSetsService;
import in.gov.nvli.harvester.services.RepositoryService;
import in.gov.nvli.harvester.utilities.CustomBeansGenerator;
import in.gov.nvli.harvester.utilities.DatesRelatedUtil;
import in.gov.nvli.harvester.utilities.HttpURLConnectionUtil;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.xml.bind.JAXBException;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@RestController
@RequestMapping(value = "/OAIPMHrepository")
public class OAIPMHRepositoryController {

    @Autowired
    public IdentifyService identifyService;

    @Autowired
    public ListMetadataFormatsService listMetadataFormatsService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private HarMetadataTypeDao harMetadataTypeDao;

    @Autowired
    private HarRepoMetadataDao harRepoMetadataDao;

    @Autowired
    private ListSetsService listSetsService;

    @Autowired
    private RepositoryDao repositoryDao;

    @Autowired
    private RepositoryHarvestService rhs;

    @Autowired
    private OAIPMHRepositoryService oaipmhrs;

    private HttpURLConnection connection;
    private List<MetadataFormatType> metaDataFormats;

    private IdentifyTypeCustomised custObj;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OAIPMHRepositoryController.class);

    @RequestMapping(value = "/identifyBaseUrl")
    public IdentifyTypeCustomised identifyBaseUrl(@RequestParam("baseURL") String baseURL, @QueryParam("adminEmail") String adminEmail) {
        try {
            IdentifyType identifyObj = identifyService.getIdentifyTypeObject(baseURL, MethodEnum.GET, adminEmail);
            connection = HttpURLConnectionUtil.getConnection(baseURL + CommonConstants.VERB + VerbType.LIST_METADATA_FORMATS.value(), MethodEnum.GET, adminEmail);
            metaDataFormats = listMetadataFormatsService.getMetadataFormatTypeList(connection, baseURL);
            custObj = CustomBeansGenerator.convertIdentifyTypeToIdentifyTypeCustomised(identifyObj);
            if (metaDataFormats != null) {
                for (MetadataFormatType metadata : metaDataFormats) {
                    if (oaipmhrs.isSupportsMetadata(metadata.getMetadataPrefix().trim().toUpperCase())) {
                        custObj.getSupportedMetadataTypes().put(HarRecordMetadataTypeEnum.valueOf(metadata.getMetadataPrefix().trim().toUpperCase()), Boolean.TRUE);
                    }
                }
            }
        } catch (IOException | JAXBException | OAIPMHerrorTypeException ex) {
            Logger.getLogger(OAIPMHRepositoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return custObj;
    }

    @RequestMapping(value = "/addRepository", method = {RequestMethod.POST})
    @ResponseBody
    public JSONObject addRepository(@RequestBody NewRepoDTO newRepoDTO, HttpServletRequest request
    ) {
        boolean response = false;
        HarRepoMetadata harRepoMetadataObj = new HarRepoMetadata();
        HarMetadataType harMetadataTypeObj;
        HarRepo repositoryObject = new HarRepo();
        repositoryObject.setRepoName(newRepoDTO.getRepositoryName());
        repositoryObject.setRepoBaseUrl(newRepoDTO.getBaseURL());
        repositoryObject.setRepoEmail(newRepoDTO.getEmail());
        repositoryObject.setRepoDesc(newRepoDTO.getDescription());
        repositoryObject.setRepoStatusId(new HarRepoStatus((short) 2));
        repositoryObject.setRepoUID(newRepoDTO.getRepoUID());
        repositoryObject.setRepoLink(newRepoDTO.getBaseURL());
        HarRepo repo = repositoryService.addRepository(repositoryObject);
        JSONObject jsno = new JSONObject();
        jsno.put("repositoryName", newRepoDTO.getRepositoryName());
        jsno.put("repoUID", newRepoDTO.getRepoUID());

        if (repo != null) {
            if (Boolean.TRUE == oaipmhrs.saveOrUpdateRecursive(repo, metaDataFormats)) {

                Map<HarRecordMetadataTypeEnum, Boolean> supportedMetadataTypes = custObj.getSupportedMetadataTypes();
                for (Map.Entry<HarRecordMetadataTypeEnum, Boolean> tempHarRecordMetadataTypeObj : supportedMetadataTypes.entrySet()) {
                    harRepoMetadataObj = new HarRepoMetadata();
                    harMetadataTypeObj = harMetadataTypeDao.getMetadataTypeByMetadatPrefix(tempHarRecordMetadataTypeObj.getKey().value());
                    harRepoMetadataObj.setMetadataTypeId(harMetadataTypeObj);
                    harRepoMetadataObj.setRepoId(repo);
                    harRepoMetadataObj.setHarvestStatus(new HarRepoStatus(RepoStatusEnum.ACTIVE.getId()));

                    if (Objects.equals(tempHarRecordMetadataTypeObj.getValue(), Boolean.TRUE)) {
                        harRepoMetadataObj.setEnableFlag(CommonConstants.ENABLED);
                    } else {
                        harRepoMetadataObj.setEnableFlag(CommonConstants.DISABLED);
                    }
                    harRepoMetadataDao.createNew(harRepoMetadataObj);
                }
                response = true;
            }
        }

        jsno.put("status", response);
        return jsno;
    }

    @RequestMapping(value = "/harvestByMetadataTypeId")
    public void harvestByMetaData(@RequestParam("repoUID") String repoUID, @RequestParam("metadataTypeName") String metadataTypeName, @RequestParam("flag") Boolean flag) {
        HarRepo harRepo = repositoryService.getRepositoryByUID(repoUID);
        System.out.println(HarRecordMetadataTypeEnum.valueOf(metadataTypeName));
        rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.valueOf(metadataTypeName), flag);
    }

    @RequestMapping(value = "/harvesteRepository", method = {RequestMethod.GET, RequestMethod.POST})
    public JSONObject harvestRepository(@RequestParam("repoUID") String repoUID) {
        HarRepo harRepo = repositoryService.getRepositoryByUID(repoUID);
        Date beforeHarvestingDate;
        try {

            LOGGER.info(HarvesterLogConstants.HARVESTING_STARTED);
            beforeHarvestingDate = new Date();
            listSetsService.saveHarSets(harRepo, MethodEnum.GET, "");
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.SETS_SAVED);

            rhs = processHarvesting(harRepo, Boolean.FALSE);

            if (rhs.error) {
                if (harRepo.getRecordCount() != null || harRepo.getRecordCount() != 0) {
                    repositoryDao.changeRepoStatus(harRepo, RepoStatusEnum.HARVEST_PROCESSING_ERROR.getId());
                } else {
                    repositoryDao.changeRepoStatus(harRepo, RepoStatusEnum.ACTIVE.getId());
                }
            } else {
                repositoryDao.changeRepoStatus(harRepo, RepoStatusEnum.HARVEST_COMPLETE.getId());
                repositoryDao.updateLastSyncStartDate(harRepo, DatesRelatedUtil.getDateInUTCFormat(beforeHarvestingDate));
                repositoryDao.updateLastSyncEndDate(harRepo, DatesRelatedUtil.getCurrentDateTimeInUTCFormat());
                repositoryDao.updateStatusDiscription(harRepo, "harvesting completed");
                repositoryDao.merge(harRepo);
            }
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.HARVESTING_FINISHED);

        } catch (Exception ex) {
            repositoryDao.changeRepoStatus(harRepo.getRepoUID(), RepoStatusEnum.HARVEST_PROCESSING_ERROR.getId());
            LOGGER.error("RepositoryUID --> " + harRepo.getRepoUID()
                    + ex.getMessage(), ex);
            repositoryDao.updateStatusDiscription(harRepo, "harvest processing error");
        }
        JSONObject jsono = new JSONObject();
        jsono.put("status", Boolean.TRUE);
        return jsono;
    }

    @RequestMapping(value = "/checkRepositoryIsExist")
    public Boolean checkRepoUIDAlredyExist(@RequestParam("repoUID") String repoUID) {
        HarRepo repo = repositoryService.getRepositoryByUID(repoUID);
        return (repo == null) ? Boolean.FALSE : Boolean.TRUE;
    }

    @RequestMapping(value = "/checkBaseURL", method = RequestMethod.GET)
    public JSONObject checkURLStatus(@RequestParam(value = "baseURL") String url) {
        String urlStatus;
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }
        boolean isDuplicate = oaipmhrs.checkDuplicateURL(url);
        urlStatus = (!isDuplicate) ? oaipmhrs.checkURLConnectivity(url) : "URL is duplicate.";

        JSONObject json = new JSONObject();
        json.put("updateStatus", urlStatus);
        return json;
    }

    @RequestMapping(value = "/repositoryList")
    public ModelAndView listRepository() {
        ModelAndView mv = new ModelAndView("repositoryList");
        List<HarRepo> repoList = repositoryService.getAllRepositories();
        mv.addObject("RepoList", repoList);
        return mv;

    }

    public RepositoryHarvestService processHarvesting(final HarRepo harRepo, final Boolean flag) {
        try {
            listMetadataFormatsService.saveHarMetadataTypes(harRepo, MethodEnum.GET, "");
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.METADATAFORMATS_SAVED);
            List<HarMetadataTypeRepository> supportedMetadata = (List<HarMetadataTypeRepository>) harRepo.getHarMetadataTypeRepositoryCollection();

            for (HarMetadataTypeRepository r : supportedMetadata) {
                String e = r.getMetadataTypeId().getMetadataPrefix();
                if (HarRecordMetadataTypeEnum.getBasedOnText(e) != null) {
                    rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.getBasedOnText(e), flag);
                }
            }

//            rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.OAI_DC, flag);
//            rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.ORE, flag);
//            rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.METS, flag);
//            rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.MARC, flag);
//            rhs.harvestRepositoryByRecordMetadataType(harRepo, HarRecordMetadataTypeEnum.MARCXML, flag);

            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.GETTING_RECORD_COUNT);
            repositoryDao.updateHarRecordCount(harRepo);
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.RECORD_COUNT_UPDATED);
        } catch (IOException | JAXBException | OAIPMHerrorTypeException ex) {
            Logger.getLogger(OAIPMHRepositoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rhs;

    }

    @RequestMapping(value = "/harvestRepositoryIncremental", method = {RequestMethod.GET, RequestMethod.POST})
    public JSONObject harvestRepositoryIncremental(@RequestParam("repoUID") String repoUID) {
        HarRepo harRepo = repositoryService.getRepositoryByUID(repoUID);
        Date beforeHarvestingDate;
        try {
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.INCREMENTAL_HARVESTING_STARTED);
            beforeHarvestingDate = new Date();

            listSetsService.saveOrUpdateHarSets(harRepo, MethodEnum.GET, "");
            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.SETS_SAVED);

            rhs = processHarvesting(harRepo, Boolean.TRUE);

            if (rhs.error) {
                repositoryDao.changeRepoStatus(harRepo.getRepoUID(), RepoStatusEnum.INCREMENT_HARVEST_PROCESSING_ERROR.getId());
                LOGGER.info("updated repo status , incremental harvest processing error");
            } else {
                repositoryDao.changeRepoStatus(harRepo, RepoStatusEnum.HARVEST_COMPLETE.getId());
                repositoryDao.updateLastSyncStartDate(harRepo, DatesRelatedUtil.getDateInUTCFormat(beforeHarvestingDate));
                repositoryDao.updateLastSyncEndDate(harRepo, DatesRelatedUtil.getCurrentDateTimeInUTCFormat());
                LOGGER.info("updated repo status , incremental harvest completed");
            }

            harRepo = repositoryDao.getRepositoryByUID(harRepo.getRepoUID());

            LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.INCREMENTAL_HARVESTING_FINISHED);
            repositoryDao.updateStatusDiscription(harRepo, "Incremented harvesting completed");
        } catch (OAIPMHerrorTypeException | IOException | ParseException | JAXBException ex) {
            repositoryDao.changeRepoStatus(harRepo.getRepoUID(), RepoStatusEnum.INCREMENT_HARVEST_PROCESSING_ERROR.getId());
            LOGGER.error("RepositoryUID --> " + harRepo.getRepoUID()
                    + ex.getMessage(), ex);
            harRepo = repositoryDao.getRepositoryByUID(harRepo.getRepoUID());
        }
        JSONObject jsono = new JSONObject();
        jsono.put("status", Boolean.TRUE);
        return jsono;
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repo/xd")
    public boolean deleteRepository(@RequestParam("repoUID") String repoUID) {
        HarRepo harRepoObj = repositoryService.getRepositoryByUID(repoUID);
        boolean status = Boolean.FALSE;
        if (harRepoObj != null) {
            status = repositoryService.deleteRepository(harRepoObj.getRepoId());
        }
        return status;
    }

}
