/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.controllers;

import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.beans.HarRepoStatus;
import in.gov.nvli.harvester.services.RepositoryService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Bhumika
 * @author Sanjay Rabidas<sanjayr@cdac.in>
 * @author Ankita Dhongde<dankita@cdac.in>
 */
@Controller
public class RepositoryController {

    @Autowired
    RepositoryService repositoryService;

    @RequestMapping("/addrepository")
    public ModelAndView addRepository(HarRepo repositoryObject) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String lastSynchString = "2000-12-30 14:55:06";
        short statusid = 1;
        ModelAndView mv = new ModelAndView("added_repository");
        try {
            repositoryObject.setRepoBaseUrl("baseurl");
            repositoryObject.setRepoEmail("email");
            repositoryObject.setRepoLink("repolink");
            //get status object based on id
            repositoryObject.setRepoStatusId(new HarRepoStatus(statusid));
            Date lastSynchDate = formatter.parse(lastSynchString);
            repositoryObject.setRepoLastSyncDate(lastSynchDate);
            if (repositoryService.addRepository(repositoryObject) != null) {
                return mv;
            }
        } catch (ParseException e) {
            Logger.getLogger(RepositoryController.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception e) {
            Logger.getLogger(RepositoryController.class.getName()).log(Level.SEVERE, null, e);
        }
        return mv;
    }
}
