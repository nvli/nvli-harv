package in.gov.nvli.harvester.test;

import in.gov.nvli.harvester.controllers.SSLClientFactory;
import in.gov.nvli.harvester.custom.harvester_enum.HarRecordMetadataTypeEnum;
import in.gov.nvli.harvester.customised.HarRepoCustomised;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.client.RestTemplate;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
public class Test {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Test.class);

    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "admin:admin";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    public short submitEntityToHarvester() {
        short statusCode = 0;
        try {
            URI webServiceURL = new URI("http://localhost:8080/harvester/rest/SaveAndHarvestRepository");
            HarRepoCustomised harRepoCustomised = new HarRepoCustomised();
            harRepoCustomised.setRepoUID("OPRE1");
            harRepoCustomised.setRepoName("Test Repo");
            harRepoCustomised.setRepoBaseUrl("http://moeseprints.incois.gov.in/cgi/oai2");
            harRepoCustomised.setRepoEmail("adminmoeseprints@incois.gov.in");
            harRepoCustomised.setRepoLink("http://moeseprints.incois.gov.in");
            harRepoCustomised.setRepoStatusId((short) 2);
            Map<HarRecordMetadataTypeEnum, Boolean> supportedMetadataTypes = new HashMap<>();
            supportedMetadataTypes.put(HarRecordMetadataTypeEnum.OAI_DC, true);
            supportedMetadataTypes.put(HarRecordMetadataTypeEnum.METS, true);

            harRepoCustomised.setSupportedMetadataTypes(supportedMetadataTypes);

            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.httpClient));

            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent", "NVLI Internal Web Service");
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<HarRepoCustomised> entity = new HttpEntity<>(harRepoCustomised, getConfiguredHttpHeaders());
            HarRepoCustomised responseHarRepoCustomised = restTemplate.postForObject(webServiceURL, entity, HarRepoCustomised.class);
            if (responseHarRepoCustomised != null) {
                statusCode = 1; //Not Active
                LOGGER.info("Repository saved successfully at Harvester.");
            } else {
                statusCode = 9; //error in save
                LOGGER.info("Repository not saved successfully at Harvester.");
            }
        } catch (URISyntaxException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statusCode;
    }

    public static void main(String[] args) {
        Test t = new Test();
        try {
            t.submitEntityToHarvester();
        } catch (Exception ex) {
            LOGGER.info("Repository saved successfully at Harvester.");
        }
    }
}
