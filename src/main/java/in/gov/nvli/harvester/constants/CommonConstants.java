/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.constants;

/**
 *
 * @author richa
 */
public class CommonConstants {

    public static String COLUMN_VALUE_SEPARATOR = "&|&";
    public static String RECORD_NAME_SEPARATOR = "_";
    public static Short RECORD_NOT_DELETED = 0;
    public static Short RECORD_DELETED = 1;

    public static final String VERB = "?verb=";
    public static final String METADATA_PREFIX = "&metadataPrefix=";
    public static final String RESUMPTION_TOKEN = "&resumptionToken=";
    public static final String IDENTIFIER = "&identifier=";
    public static final String FROM = "&from=";
    public static final String PATH_PARAM_SEPERATOR = ",";

    public static final String DIRECTORY_Name_ORE = "ore";
    public static final String DIRECTORY_Name_OAI_DC = "oai_dc";
    public static final String DIRECTORY_Name_MARC = "";    //marc & marcxml files will be placed directly in root folder hence blank
    public static final String DIRECTORY_Name_MARCXML = "";
    public static final String DIRECTORY_Name_METS = "mets";
    public static final String DIRECTORY_Name_RECORD_DATA = "data";

    public static final String WEB_SERVICES_LOG_MESSAGES = "Web Service Logs :";

    public static final short ENABLED = 1;
    public static final short DISABLED = 0;

}
