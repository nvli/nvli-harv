/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.restResources;

import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.custom.harvester_enum.RepoStatusEnum;
import in.gov.nvli.harvester.customised.HarRepoCustomised;
import in.gov.nvli.harvester.dao.RepositoryDao;
import in.gov.nvli.harvester.services.HarvesterService;
import in.gov.nvli.harvester.services.RepositoryService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vootla
 * @author Ankita Dhongde
 */
@RestController
@RequestMapping("/rest")
@Secured("ROLE_ADMIN")
public class HarvesterResource {

    @Autowired
    private HarvesterService harvesterService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RepositoryDao repositoryDao;

    @Autowired
    private RepositoryResource repositoryResource;

    /**
     *
     * @return harvested repositories
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest")
    public Map<String, String> startHarvestAll() {
        List<HarRepo> activeRepos = repositoryService.getActiveRepositories();
        Map<String, String> result = new HashMap<>();
        if (activeRepos == null) {
            result.put("message", "no Active Repositories,Please Active & Try Again");

        } else {
            harvesterService.harvestAllActiveRepositories(activeRepos);
            for (HarRepo repo : activeRepos) {
                // result.put(repo.getRepoUID(), getHarvesterStatus(repo.getRepoUID()));
                result.put(repo.getRepoUID(), RepoStatusEnum.HARVEST_PROCESSING.getName());
            }

        }
        return result;

    }

    /**
     *
     * @param repoUIDS
     * @return harvested repositories list
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest/list/{repoUIDS}")
    public Map<String, String> startHarvestList(@PathVariable("repoUIDS") String repoUIDS) {
        Map<String, String> result = new HashMap<>();
        List<String> repoUIDSList = new ArrayList<>(Arrays.asList(repoUIDS.split(",")));
        List<HarRepo> harRepos = repositoryService.getRepositoriesByUIDS(repoUIDSList);

        if (harRepos == null) {
            result.put("message", "no Repositories Exist on corresponding repoUIDS");
        } else {
            harvesterService.harvestRepositories(harRepos);
            for (String repoUID : repoUIDSList) {
                // result.put(repoUID, getHarvesterStatus(repoUID));
                result.put(repoUID, RepoStatusEnum.HARVEST_PROCESSING.getName());
            }
        }

        return result;
    }

    /**
     *
     * @param repoUID
     * @return repository status {@link RepoStatusEnum }
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest/{repoUID}")
    public String startHarvesting(@PathVariable("repoUID") String repoUID) {
        harvesterService.harvestRepositoryByUID(repoUID);
        return RepoStatusEnum.HARVEST_PROCESSING.getName();
    }

    //new method
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/SaveAndHarvestRepository")
    public String saveAndHarvestRepository(@RequestBody HarRepoCustomised harRepoCustomised) {
        String repoUID = harRepoCustomised.getRepoUID();
        HarRepo harRepo = repositoryDao.getRepositoryByUID(repoUID);
        if (harRepo != null) {
            Response updateResponse = repositoryResource.updateNewRepository(repoUID, harRepoCustomised);
            if (updateResponse.getStatus() == 200) {
                if (harvesterService.harvestRepositoryByUID(repoUID)) {
                    return RepoStatusEnum.HARVEST_PROCESSING.getName();
                } else {
                    return RepoStatusEnum.HARVEST_PROCESSING_ERROR.getName();
                }
            }
        } else {
            HarRepoCustomised harRpo1 = repositoryResource.saveNewRepository(harRepoCustomised);
            if (harRpo1 != null) {
                if (harvesterService.harvestRepositoryByUID(repoUID)) {
                    return RepoStatusEnum.HARVEST_PROCESSING.getName();
                } else {
                    return RepoStatusEnum.HARVEST_PROCESSING_ERROR.getName();
                }
            } else {
                return RepoStatusEnum.UNABLE_TO_PROCESS.getName();
            }
        }
        return RepoStatusEnum.UNABLE_TO_PROCESS.getName();
    }

    /**
     *
     * @param repoUID
     * @return repository status
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest/status/{repoUID}")
    public String getHarvesterStatus(@PathVariable("repoUID") String repoUID) {
        return repositoryService.getRepositoryByUID(repoUID).getRepoStatusId().getRepoStatusName();
    }

    /**
     *
     * @return repository status
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest/status")
    public Map<String, String> getAllRepoStatus() {
        List<HarRepo> harRepos = repositoryService.getAllRepositories();
        return reposStatusInternal(harRepos);
    }

    /**
     *
     * @param repoUIDS
     * @return repository status
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/harvest/status/list/{repoUIDS}")
    public Map<String, String> getListOfRepoStatus(@PathVariable("repoUIDS") String repoUIDS) {

        List<String> repoUIDSList = new ArrayList<>(Arrays.asList(repoUIDS.split(",")));
        List<HarRepo> harRepos = repositoryService.getRepositoriesByUIDS(repoUIDSList);
        return reposStatusInternal(harRepos);
    }

    private Map<String, String> reposStatusInternal(List<HarRepo> harRepos) {
        Map<String, String> result = new HashMap<String, String>();
        if (harRepos == null) {
            result.put("message", "no Repositories Exist");
        } else {
            for (HarRepo repo : harRepos) {
                result.put(repo.getRepoUID(), repo.getRepoStatusId().getRepoStatusName());
            }
        }

        return result;
    }

}
