/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.restResources;

import in.gov.nvli.harvester.beans.HarMetadataTypeRepository;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.beans.HarRepoMetadata;
import in.gov.nvli.harvester.beans.HarRepoStatus;
import in.gov.nvli.harvester.constants.CommonConstants;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import in.gov.nvli.harvester.customised.HarRepoCustomised;
import in.gov.nvli.harvester.custom.harvester_enum.RepoStatusEnum;
import in.gov.nvli.harvester.dao.HarRepoMetadataDao;
import in.gov.nvli.harvester.services.ListMetadataFormatsService;
import in.gov.nvli.harvester.services.RepositoryMetadataService;
import in.gov.nvli.harvester.services.RepositoryService;
import in.gov.nvli.harvester.utilities.CustomBeansGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vootla
 * @author Ankita Dhongde
 */
@RestController
@RequestMapping("/rest")
@Secured("ROLE_ADMIN")
public class RepositoryResource {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RepositoryMetadataService repositoryMetadataServiceObj;

    @Autowired
    private HarRepoMetadataDao harRepoMetadataDao;

    @javax.ws.rs.core.Context
    ServletContext context;

    /**
     * The name of the property LOGGER used to holds {@link Logger} object
     * reference.
     */
    private final static Logger LOGGER = Logger.getLogger(RepositoryResource.class);

    /**
     *
     * @param harRepoCustomised
     * @return Customized harvested repository object
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories")
    public HarRepoCustomised saveRepositoryObject(@RequestBody HarRepoCustomised harRepoCustomised) {
        return saveRepository(harRepoCustomised);
    }

    public HarRepoCustomised saveNewRepository(HarRepoCustomised harRepoCustomised) {
        return saveRepository(harRepoCustomised);
    }

    /**
     *
     * @return list of repositories
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/list")
    public List<HarRepoCustomised> repositoriesList() {
        return repositories();
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/list/update")
    public List<HarRepoCustomised> repositoriesListWithMetadataUpdate() {
        return repositories();
    }

    /**
     *
     * @param repoUID
     * @return repository status
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/activate/{repoUID}")
    public String repositoriesActivateParticular(@PathVariable("repoUID") String repoUID) {
        return changeRepoStatus(repoUID, RepoStatusEnum.ACTIVE.getId());
    }

    /**
     *
     * @param repoUID
     * @return repository status
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/deactivate/{repoUID}")
    public String repositoriesDeActivateParticular(@PathVariable("repoUID") String repoUID) {
        return changeRepoStatus(repoUID, RepoStatusEnum.NOT_ACTIVE.getId());
    }

    /**
     *
     * @param repoUID
     * @param harRepoCustomised
     * @return response of updated repository
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT}, value = "/repositories/{repoUID}")
    public Response updateRepositoryGetJ(@PathVariable("repoUID") String repoUID, @RequestBody HarRepoCustomised harRepoCustomised) {
        return updateRepository(repoUID, harRepoCustomised);
    }

    public Response updateNewRepository(String repoUID, HarRepoCustomised harRepoCustomised) {
        return updateRepository(repoUID, harRepoCustomised);
    }

    /**
     *
     * @param repoUID
     * @return response of updated repository
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/getRepo/{repoUID}")
    public Response getRepositoryJSON(@PathVariable("repoUID") String repoUID) {
        return getRepository(repoUID);
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/delete/{repoUID}")
    public boolean deleteRepository(@PathVariable("repoUID") String repoUID) {
        HarRepo harRepoObj = repositoryService.getRepositoryByUID(repoUID);
        if (harRepoObj != null) {
            return repositoryService.deleteRepository(harRepoObj.getRepoId());
        } else {
            return true;
        }
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/repositories/getMetadataStatus/{repoUID}")
    @ResponseBody
    public JSONObject getMetadataStatus(@PathVariable("repoUID") String repoUID) {
        HarRepo harRepoObj = repositoryService.getRepositoryByUID(repoUID);
        List<HarRepoMetadata> harRepoMetadataObjList = repositoryMetadataServiceObj.list(harRepoObj);

        JSONObject jsono = new JSONObject();
        if (harRepoMetadataObjList != null) {
            List<JSONObject> harRepoMetadataObjList1 = new ArrayList<>();
            for (HarRepoMetadata repoMetadata : harRepoMetadataObjList) {
                JSONObject o = new JSONObject();
                o.put("metadata_name", repoMetadata.getMetadataTypeId().getMetadataPrefix());
                o.put("status_name", repoMetadata.getHarvestStatus().getRepoStatusName());
                o.put("status_code", repoMetadata.getHarvestStatus().getRepoStatusId());
                o.put("status_desc", repoMetadata.getHarvestDiscription());
                o.put("metadata_type_id", repoMetadata.getMetadataTypeId().getMetadataId());
                harRepoMetadataObjList1.add(o);
            }
            jsono.put("metadata", harRepoMetadataObjList1);
            jsono.put("repo_status_discription", harRepoObj.getStatusDescription());
        }
        return jsono;
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/repositories/getHarRepoStatus/{repoUID}")
    @ResponseBody
    public JSONObject getHarRepoStatus(@PathVariable("repoUID") String repoUID) {
        HarRepo harRepoObj = repositoryService.getRepositoryByUID(repoUID);
        JSONObject jsono = new JSONObject();
        if (harRepoObj != null) {
            jsono.put("status_desc", harRepoObj.getStatusDescription());
        } else {
            jsono.put("message", "No records found");
        }
        return jsono;
    }

    public String changeRepoStatus(short status) {
        return RepoStatusMsg(repositoryService.changeRepoStatus(status), status);
    }

    public List<String> splitRepoUIDS(String repoUIDS) {
        List<String> repoUIDSList = new ArrayList<>(Arrays.asList(repoUIDS.split(CommonConstants.PATH_PARAM_SEPERATOR)));
        return repoUIDSList;
    }

    private String changeRepoStatus(String repositoryUID, short status) {
        HarRepo harRepoObj = repositoryService.getRepositoryByUID(repositoryUID);
        if (status == RepoStatusEnum.ACTIVE.getId()) {
            if (harRepoObj.getRepoStatusId().getRepoStatusId() == RepoStatusEnum.NOT_ACTIVE.getId()
                    || harRepoObj.getRepoStatusId().getRepoStatusId() == RepoStatusEnum.INVALID_URL.getId()) {
                return RepoStatusMsg(repositoryService.changeRepoStatus(repositoryUID, status), status);
            } else if (harRepoObj.getRepoStatusId().getRepoStatusId() == RepoStatusEnum.ACTIVE.getId()) {
                return RepoStatusEnum.ACTIVE.getName();
            }
        } else if (status == RepoStatusEnum.NOT_ACTIVE.getId()) {
            if (harRepoObj.getRepoStatusId().getRepoStatusId() == RepoStatusEnum.ACTIVE.getId()) {
                return RepoStatusMsg(repositoryService.changeRepoStatus(repositoryUID, status), status);
            } else if (harRepoObj.getRepoStatusId().getRepoStatusId() == RepoStatusEnum.NOT_ACTIVE.getId()) {
                return RepoStatusEnum.NOT_ACTIVE.getName();
            }
        }
        return "Invalid Request";
    }

    //returns the status of repositry after changing it
    public String changeRepoStatus(List<String> repoUIDSList, short status) {
        return RepoStatusMsg(repositoryService.changeRepoStatus(repoUIDSList, status), status);
    }

    private String RepoStatusMsg(boolean result, int status) {
        if (result) {
            if (status == RepoStatusEnum.ACTIVE.getId()) {
                return RepoStatusEnum.ACTIVE.getName();

            }
            if (status == RepoStatusEnum.NOT_ACTIVE.getId()) {
                return RepoStatusEnum.NOT_ACTIVE.getName();
            }
            return "ERROR";

        } else {
            return "ERROR";
        }
    }

    private HarRepoCustomised saveRepository(HarRepoCustomised harRepoCustomised) {
        HarRepoCustomised harRepoCustomisedObj = null;
        HarRepo repo = CustomBeansGenerator.convertHarRepoCustomisedToHarRepo(harRepoCustomised);
        repo = repositoryService.addRepository(repo);
        if (repo != null) {
            if (repositoryMetadataServiceObj.saveRepositoryMetadata(harRepoCustomised, MethodEnum.GET, "")) {
                List<HarRepoMetadata> harRepoMetadataList = repositoryMetadataServiceObj.list(repo);
                return CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo, harRepoMetadataList);
            }
        }
        return harRepoCustomisedObj;
    }

    private List<HarRepoCustomised> repositories() {
        List<HarRepoCustomised> harRepoCustomisedList = new ArrayList<>();
        List<HarRepo> harRepos = repositoryService.getAllRepositories();
        List<HarRepoMetadata> harRepoMetadataList;
        for (HarRepo repo : harRepos) {
            harRepoMetadataList = repositoryMetadataServiceObj.list(repo);
            harRepoCustomisedList.add(CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo, harRepoMetadataList));

        }
        return harRepoCustomisedList;
    }

    public void updateRepositoryMetadataStatus() {
        List<HarRepo> harRepos = repositoryService.getAllRepositories();
        List<HarRepoMetadata> harRepoMetadataList;
        for (HarRepo repo : harRepos) {
            HarRepoStatus repoStatusId = repo.getRepoStatusId();
            harRepoMetadataList = harRepoMetadataDao.list(repo);
            for (HarRepoMetadata harRepoMetadata : harRepoMetadataList) {
                harRepoMetadata.setHarvestStatus(repoStatusId);
            }
        }
    }

    private Response updateRepository(String repoUID, HarRepoCustomised custObj) {
        HarRepo repoOrginal = repositoryService.getRepositoryByUID(repoUID);

        if (repoOrginal == null) {
            return Response.status(400).entity("Repository with repoUID" + repoUID + "not Exist !!").build();
        }
        if (custObj == null) {
            return Response.status(400).entity("Please provide the  HarRepoCustomised object !!").build();
        }
        HarRepo repo = CustomBeansGenerator.convertHarRepoCustomisedToHarRepo(custObj);

        repo.setRepoUID(repoOrginal.getRepoUID());
        repo.setRepoBaseUrl(repoOrginal.getRepoBaseUrl());
        repo.setRepoId(repoOrginal.getRepoId());
//        repo.setRepoActivationDate(repoOrginal.getRepoActivationDate());
        repo.setRepoLastSyncDate(repoOrginal.getRepoLastSyncDate());
//        repo.setRepoRegistrationDate(repoOrginal.getRepoRegistrationDate());
        repo.setRepoRowUpdateTime(repoOrginal.getRepoRowUpdateTime());
        repo.setRecordCount(repoOrginal.getRecordCount());
        repo.setRepoLastSyncEndDate(repoOrginal.getRepoLastSyncEndDate());

        repositoryService.editRepository(repo);
        repositoryMetadataServiceObj.saveOrUpdateRepositoryMetadata(custObj, repo);

        List<HarRepoMetadata> harRepoMetadataList = repositoryMetadataServiceObj.list(repo);
        custObj = CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo, harRepoMetadataList);

        return Response.ok().entity(custObj).build();
    }

    private Response getRepository(String repoUID) {
        HarRepo repo = repositoryService.getRepositoryByUID(repoUID);
        HarRepoCustomised custObj;
        if (repo == null) {
            return Response.status(400).entity("Repository with repoUID" + repoUID + "not Exist !!").build();
        }
        repositoryService.saveHarMetadataTypes(repo);
        List<HarRepoMetadata> harRepoMetadataList = repositoryMetadataServiceObj.list(repo);
        if (harRepoMetadataList.isEmpty()) {
            List<HarMetadataTypeRepository> harMetadataTypeRepositoryList = repositoryService.list(repo);
            custObj = CustomBeansGenerator.convertHarRepoToHarRepoCustomisedByHarMetadataTypeRepository(repo, harMetadataTypeRepositoryList);
        } else {
            custObj = CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo, harRepoMetadataList);
        }

        return Response.ok().entity(custObj).build();
    }
}
