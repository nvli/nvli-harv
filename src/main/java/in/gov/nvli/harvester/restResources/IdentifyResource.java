/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.restResources;

import in.gov.nvli.harvester.oaipmhbeans.IdentifyType;
import in.gov.nvli.harvester.oaipmhbeans.MetadataFormatType;
import in.gov.nvli.harvester.oaipmhbeans.VerbType;
import in.gov.nvli.harvester.constants.CommonConstants;
import in.gov.nvli.harvester.custom.exception.OAIPMHerrorTypeException;
import in.gov.nvli.harvester.custom.harvester_enum.HarRecordMetadataTypeEnum;
import in.gov.nvli.harvester.customised.IdentifyTypeCustomised;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import in.gov.nvli.harvester.services.IdentifyService;
import in.gov.nvli.harvester.services.ListMetadataFormatsService;
import in.gov.nvli.harvester.utilities.CustomBeansGenerator;
import in.gov.nvli.harvester.utilities.HttpURLConnectionUtil;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.util.List;
import javax.ws.rs.QueryParam;
import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vootla
 * @author Ankita Dhongde
 */
@RestController
@RequestMapping("/rest")
@Secured("ROLE_ADMIN")
public class IdentifyResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdentifyResource.class);

    @Autowired
    public IdentifyService identifyService;

    @Autowired
    public ListMetadataFormatsService listMetadataFormatsService;

    /**
     *
     * @param baseURL
     * @param adminEmail
     * @return customized identity object
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/identifies/{baseURL}/{adminEmail}")
    public IdentifyTypeCustomised identifyPathParam(@PathVariable("baseURL") String baseURL, @PathVariable("adminEmail") String adminEmail) {
        return identify(baseURL, adminEmail);
    }

    /**
     *
     * @param baseURL
     * @param adminEmail
     * @return customized identity object
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/identifies/{baseURL}")
    public IdentifyTypeCustomised identifyPathParam1(@PathVariable("baseURL") String baseURL, @QueryParam("adminEmail") String adminEmail) {
        return identify(baseURL, adminEmail);
    }

    /**
     *
     * @param baseURL
     * @param adminEmail
     * @return customized identity object
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/identifies")
    public IdentifyTypeCustomised identifyPathParam2(@RequestParam("baseURL") String baseURL, @QueryParam("adminEmail") String adminEmail) {
        return identify(baseURL, adminEmail);
    }

    private IdentifyTypeCustomised identify(String baseURL, String adminEmail) {
        IdentifyTypeCustomised custObj;

        LOGGER.info(CommonConstants.WEB_SERVICES_LOG_MESSAGES
                + "\nActivity --> " + VerbType.IDENTIFY.value()
                + "\nBase URL --> " + baseURL);

        try {
            baseURL = URLDecoder.decode(baseURL, "UTF-8");
            // adminEmail = URLDecoder.decode(adminEmail, "UTF-8");
            IdentifyType identifyObj = identifyService.getIdentifyTypeObject(baseURL, MethodEnum.GET, adminEmail);
            HttpURLConnection connection = HttpURLConnectionUtil.getConnection(baseURL + CommonConstants.VERB + VerbType.LIST_METADATA_FORMATS.value(), MethodEnum.GET, adminEmail);
            List<MetadataFormatType> metaDataFormats = listMetadataFormatsService.getMetadataFormatTypeList(connection, baseURL);
            custObj = CustomBeansGenerator.convertIdentifyTypeToIdentifyTypeCustomised(identifyObj);
            if (metaDataFormats != null) {
                for (MetadataFormatType metadata : metaDataFormats) {
                    try {
                        custObj.getSupportedMetadataTypes().put(HarRecordMetadataTypeEnum.valueOf(metadata.getMetadataPrefix().trim().toUpperCase()), Boolean.FALSE);
                    } catch (IllegalArgumentException ex) {
                        LOGGER.error(CommonConstants.WEB_SERVICES_LOG_MESSAGES + metadata.getMetadataPrefix() + " is not supported by System");
                    }
                }
            }
            return custObj;
        } catch (IOException | JAXBException | OAIPMHerrorTypeException ex) {
            custObj = new IdentifyTypeCustomised();
            custObj.setErrorMessage(ex.getMessage());
            LOGGER.error(CommonConstants.WEB_SERVICES_LOG_MESSAGES
                    + "\nActivity --> " + VerbType.IDENTIFY.value()
                    + "\nBase URL --> " + baseURL
                    + "\nError --> " + ex.getMessage(), ex);
        } catch (Exception ex) {
            custObj = new IdentifyTypeCustomised();
            custObj.setErrorMessage(ex.getMessage());
            LOGGER.error(CommonConstants.WEB_SERVICES_LOG_MESSAGES
                    + "\nActivity --> " + VerbType.IDENTIFY.value()
                    + "\nBase URL --> " + baseURL
                    + "\nError --> " + ex.getMessage(), ex);
        }
        return custObj;
    }
}
