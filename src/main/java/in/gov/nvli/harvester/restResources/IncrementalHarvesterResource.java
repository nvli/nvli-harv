/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.restResources;

import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.custom.harvester_enum.RepoStatusEnum;
import in.gov.nvli.harvester.customised.HarRepoCustomised;
import in.gov.nvli.harvester.dao.RepositoryDao;
import in.gov.nvli.harvester.services.HarvesterService;
import in.gov.nvli.harvester.services.RepositoryService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vootla
 * @author Ankita Dhongde
 */
@RestController
@RequestMapping("/rest")
@Secured("ROLE_ADMIN")
public class IncrementalHarvesterResource {

    @Autowired
    private HarvesterService harvesterService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RepositoryDao repositoryDao;

    @Autowired
    private RepositoryResource repositoryResource;

    /**
     *
     * @return result set of incremental harvested repositories
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/incremental_harvest")
    public Map<String, String> startIncrHarvestAll() {
        List<HarRepo> activeRepos = repositoryService.getRepositoriesByStaus(RepoStatusEnum.HARVEST_COMPLETE.getId());
        Map<String, String> result = new HashMap<>();
        if (activeRepos == null) {
            result.put("message", "no Active Repositories,Please Active & Try Again");

        } else {
            harvesterService.harvestRepositoriesIncremental(activeRepos);
            for (HarRepo repo : activeRepos) {
                // result.put(repo.getRepoUID(), getHarvesterStatus(repo.getRepoUID()));
                result.put(repo.getRepoUID(), RepoStatusEnum.INCREMENT_HARVEST_PROCESSING.getName());
            }

        }
        return result;

    }

    /**
     *
     * @param repoUIDS
     * @return result set of incremental harvested repositories
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/incremental_harvest/list/{repoUIDS}")
    public Map<String, String> startIncrHarvestList(@PathVariable("repoUIDS") String repoUIDS) {
        Map<String, String> result = new HashMap<>();
        List<String> repoUIDSList = new ArrayList<>(Arrays.asList(repoUIDS.split(",")));
        List<HarRepo> harRepos = repositoryService.getRepositoriesByUIDS(repoUIDSList);

        if (harRepos == null) {
            result.put("message", "no Repositories Exist on corresponding repoUIDS");
        } else {
            harvesterService.harvestRepositoriesIncremental(harRepos);
            for (String repoUID : repoUIDSList) {
                // result.put(repoUID, getHarvesterStatus(repoUID));
                result.put(repoUID, RepoStatusEnum.INCREMENT_HARVEST_PROCESSING.getName());
            }
        }

        return result;
    }

    /**
     *
     * @param repoUID
     * @return result set of incremental harvested repositories
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/incremental_harvest/{repoUIDS}")
    public String startIncrHarvesting(@PathVariable("repoUIDS") String repoUID) {
        harvesterService.harvestRepositoryIncrementalBYUID(repoUID);
        return RepoStatusEnum.INCREMENT_HARVEST_PROCESSING.getName();
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/UpdateAndStartIncremantalHarvesting")
    public String saveAndHarvestRepository(@RequestBody HarRepoCustomised harRepoCustomised) {
        String repoUID = harRepoCustomised.getRepoUID();
        HarRepo harRepo = repositoryDao.getRepositoryByUID(repoUID);
        if (harRepo != null) {
            Response updateResponse = repositoryResource.updateNewRepository(repoUID, harRepoCustomised);
            if (updateResponse.getStatus() == 200) {
                if (harvesterService.harvestRepositoryIncrementalBYUID(repoUID)) {
                    return RepoStatusEnum.INCREMENT_HARVEST_PROCESSING.getName();
                } else {
                    return RepoStatusEnum.INCREMENT_HARVEST_PROCESSING_ERROR.getName();
                }
            } else {
                return RepoStatusEnum.UNABLE_TO_PROCESS.getName();
            }

        } else {
            return RepoStatusEnum.UNABLE_TO_PROCESS.getName();
        }

    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, value = "/incremental_harvesting")
    public String startIncrHarvesting(@RequestBody HarRepoCustomised harRepoCustomised) {
        String repoUID = harRepoCustomised.getRepoUID();
        harvesterService.harvestRepositoryIncrementalBYUID(repoUID);
        return RepoStatusEnum.INCREMENT_HARVEST_PROCESSING.getName();
    }

}
