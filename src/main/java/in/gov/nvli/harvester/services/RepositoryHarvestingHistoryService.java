package in.gov.nvli.harvester.services;

import java.util.List;

/**
 * This class has methods which are important for repository harvesting history
 * related events like saving history,deleting history etc.
 *
 * @author Bhumika
 */
//@Service
public interface RepositoryHarvestingHistoryService {

    /**
     *
     * @param repositoryIds
     */
    void deleteHistoryOfRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     * @return
     */
    boolean deleteHistoryOfRepository(int repositoryId);

    /**
     *
     * @param historyIds
     */
    void deleteHistoryByIds(List<Integer> historyIds);

    /**
     *
     * @param historyId
     * @return
     */
    boolean deleteParticularHistoryById(int historyId);

}
