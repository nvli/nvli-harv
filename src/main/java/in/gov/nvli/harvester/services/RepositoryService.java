package in.gov.nvli.harvester.services;

import in.gov.nvli.harvester.beans.HarMetadataTypeRepository;
import in.gov.nvli.harvester.beans.HarRepo;
import java.util.List;
import org.json.simple.JSONObject;

/**
 * This class has methods which are important for repository related events like
 * saving,deleting,updating,publishing,blocking etc repositories.
 *
 * @author Bhumika
 */
//@Service
public interface RepositoryService {

    /**
     *
     * @param repositoryObject
     * @return HarRepo
     */
    HarRepo addRepository(HarRepo repositoryObject);

    /**
     *
     * @return list of all repositories
     */
    List<HarRepo> getAllRepositories();

    /**
     *
     * @param repositoryId
     * @return repository object
     */
    HarRepo getRepository(int repositoryId);

    /**
     *
     * @param statusIds
     * @return repository object by status
     */
    List<HarRepo> getRepositoriesByStatus(List<Integer> statusIds);

    /**
     *
     * @param repositoryObject
     */
    void editRepository(HarRepo repositoryObject);

    /**
     *
     * @param repositoryId
     * @return status of deleted repository object
     */
    boolean deleteRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void deleteRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryIds
     */
    void activateOrDeactivateRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     */
    void activateOrDeactivateRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void publishOrWithdrawRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     */
    void publishOrWithdrawRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void blockRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     */
    void blockRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void synchroniseRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     */
    void synchroniseRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void scheduleSynchronisationOfRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     */
    void scheduleSynchronisationOfRepository(int repositoryId);

    /**
     *
     * @param repositoryIds
     */
    void validateRepositories(List<Integer> repositoryIds);

    /**
     *
     * @param repositoryId
     * @return status of valid repository object
     */
    boolean isRepositoryValid(int repositoryId);

    /**
     *
     * @param repoUID
     * @return Harvested Repositories by UID
     */
    public HarRepo getRepositoryByUID(String repoUID);

    /**
     *
     * @param repoUIDS
     * @return List of harvested repositories by UIDs
     */
    public List<HarRepo> getRepositoriesByUIDS(List<String> repoUIDS);

    /**
     *
     * @param repositoryUIDs
     * @param status
     * @return status of changed repository object
     */
    boolean changeRepoStatus(List<String> repositoryUIDs, short status);

    /**
     *
     * @param repositoryUID
     * @param status
     * @return
     */
    boolean changeRepoStatus(String repositoryUID, short status);

    /**
     *
     * @param status
     * @return
     */
    public boolean changeRepoStatus(short status);

    /**
     *
     * @return
     */
    public List<HarRepo> getActiveRepositories();

    /**
     *
     * @param repoStatusId
     * @return
     */
    public List<HarRepo> getRepositoriesByStaus(short repoStatusId);

    /**
     *
     * @param harRepoObj
     * @return
     */
    public List<HarMetadataTypeRepository> list(HarRepo harRepoObj);

    /**
     *
     * @param harRepoObj
     */
    public void saveHarMetadataTypes(HarRepo harRepoObj);

    /**
     *
     * @param repo_uid
     * @return
     */
    public JSONObject getRepoDetails(String repo_uid);

    /**
     *
     * @param baseUrl
     * @return
     */
    public HarRepo getRepository(String baseUrl);
}
