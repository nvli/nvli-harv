/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.services;

import in.gov.nvli.harvester.oaipmhbeans.IdentifyType;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.xml.bind.JAXBException;

/**
 *
 * @author vootla
 */
//@Service
public interface IdentifyService {

    /**
     *
     * @param baseURL
     * @param method
     * @param adminEmail
     * @return harvested repository
     * @throws MalformedURLException
     * @throws IOException
     * @throws JAXBException
     */
    public HarRepo getIdentify(String baseURL, MethodEnum method, String adminEmail) throws MalformedURLException, IOException, JAXBException;

    /**
     *
     * @param baseURL
     * @param method
     * @param adminEmail
     * @return object {@link IdentifyType }
     * @throws MalformedURLException
     * @throws IOException
     * @throws JAXBException
     */
    public IdentifyType getIdentifyTypeObject(String baseURL, MethodEnum method, String adminEmail) throws MalformedURLException, IOException, JAXBException;

    /**
     *
     * @param identifyTypeObject
     * @return harvested repository
     */
    public HarRepo convertIdentifyTypeToHarRepo(IdentifyType identifyTypeObject);

}
