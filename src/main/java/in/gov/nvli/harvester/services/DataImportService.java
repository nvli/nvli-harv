/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.services;

import in.gov.nvli.harvester.dao.DataImportDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Service
public class DataImportService {

    @Autowired
    private DataImportDao dataImportDAO;

    public boolean harRepoStatusDataImportInDB() {

        return dataImportDAO.harRepoStatusDataImportInDB();

    }

}
