/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.restClient;

import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.controllers.SSLClientFactory;
import in.gov.nvli.harvester.customised.HarRepoCustomised;
import in.gov.nvli.harvester.services.RepositoryService;
import in.gov.nvli.harvester.utilities.CustomBeansGenerator;
import java.net.URISyntaxException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author vootla
 */
@Component
@RequestMapping("/client")
public class RepositoryClient {

    @Value("${nvli.client.url}")
    private String nvliClientURL;

    @Autowired
    private RepositoryService repositoryService;

    private static final Logger LOGGER = Logger.getLogger(RepositoryClient.class);

    public String getNvliClientURL() {
        return nvliClientURL;
    }

    public void setNvliClientURL(String nvliClientURL) {
        this.nvliClientURL = nvliClientURL;
    }

    private final static String updateRepositoryStatusURL = "update/resource/status/";
    private final static String updateRepositoryRecordCountURL = "update/record/count/";
    private final static String updateHrvestStartTimesURL = "update/resource/harvest/starttime/";
    private final static String updateHrvestEndTimesURL = "update/resource/harvest/endtime/";

    @RequestMapping("/update/{repoUID}")
    public void test(@PathVariable("repoUID") String repoUID) throws URISyntaxException {
        HarRepo repo = repositoryService.getRepositoryByUID(repoUID);
        LOGGER.info("in update" + repo.getRecordCount());
        synRepoWithClient(repo);

    }

    public void updateRepositoryStatus(HarRepo repo) throws URISyntaxException {
        LOGGER.info("updateRepositoryStatusURL" + updateRepositoryStatusURL);
        updateRepository(repo, updateRepositoryStatusURL);
    }

    public void updateRepositoryRecordCount(HarRepo repo) throws URISyntaxException {
        updateRepository(repo, updateRepositoryRecordCountURL);
    }

    public void updateHarvestStartTime(HarRepo repo) throws URISyntaxException {
        updateRepository(repo, updateHrvestStartTimesURL);
    }

    public void updateHarvestEndTime(HarRepo repo) throws URISyntaxException {
        updateRepository(repo, updateHrvestEndTimesURL);
    }

    public void synRepoWithClient(HarRepo repo) throws URISyntaxException {
        updateRepositoryStatus(repo);
        updateRepositoryRecordCount(repo);
        updateHarvestStartTime(repo);
        updateHarvestEndTime(repo);

    }

    private void updateRepository(HarRepo repo, String serviceURL) throws URISyntaxException {
        HarRepoCustomised harRepoCustomised = null;
        String repoUID = null;
        if (repo != null) {
            repoUID = repo.getRepoUID();
            harRepoCustomised = CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo);
        }
        String url = getNvliClientURL() + serviceURL + repoUID;
        LOGGER.info("url" + url);
        LOGGER.info("harRepoCustomized --> " + harRepoCustomised);
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.httpClient));

        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "NVLI Internal Web Service");
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HarRepoCustomised> entity = new HttpEntity<>(harRepoCustomised, headers);
        // restTemplate.put(url, entity);
        LOGGER.info("Update Repository ::" + url);
        HarRepoCustomised response = restTemplate.postForObject(url, entity, HarRepoCustomised.class);
        LOGGER.info("re" + response);
    }

    @RequestMapping("/test/{repoUID}")
    public void test1(@PathVariable("repoUID") String repoUID) {
        LOGGER.info("in client");
        HarRepo repo = repositoryService.getRepositoryByUID(repoUID);
        HarRepoCustomised harRepoCustomised = CustomBeansGenerator.convertHarRepoToHarRepoCustomised(repo);
//        harRepoCustomised.setRepoName("tetsnew");
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "NVLI Internal Web Service");
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.httpClient));
        //  headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<HarRepoCustomised> entity = new HttpEntity<>(harRepoCustomised, headers);
        ResponseEntity<HarRepoCustomised> response = restTemplate.exchange("http://10.208.27.137:8080/nvliHarvester/rest/repositories/OR202", HttpMethod.PUT, entity, HarRepoCustomised.class);
        harRepoCustomised = response.getBody();
        LOGGER.info("resonse code" + response.getStatusCode());
    }
}
