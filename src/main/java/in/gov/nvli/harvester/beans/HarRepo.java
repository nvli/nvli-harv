/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ankit
 */
@Entity
@Table(name = "har_repo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HarRepo.findAll", query = "SELECT h FROM HarRepo h")
    ,
    @NamedQuery(name = "HarRepo.findByRepoId", query = "SELECT h FROM HarRepo h WHERE h.repoId = :repoId")
    ,
    @NamedQuery(name = "HarRepo.findByRepoUID", query = "SELECT h FROM HarRepo h WHERE h.repoUID = :repoUID")
    ,
    @NamedQuery(name = "HarRepo.findByRepoName", query = "SELECT h FROM HarRepo h WHERE h.repoName = :repoName")
    ,
    @NamedQuery(name = "HarRepo.findByRepoBaseUrl", query = "SELECT h FROM HarRepo h WHERE h.repoBaseUrl = :repoBaseUrl")
    ,
    @NamedQuery(name = "HarRepo.findByRepoEarliestTimestamp", query = "SELECT h FROM HarRepo h WHERE h.repoEarliestTimestamp = :repoEarliestTimestamp")
    ,
    @NamedQuery(name = "HarRepo.findByRepoGranularityDate", query = "SELECT h FROM HarRepo h WHERE h.repoGranularityDate = :repoGranularityDate")
    ,
    @NamedQuery(name = "HarRepo.findByRepoDeletionMode", query = "SELECT h FROM HarRepo h WHERE h.repoDeletionMode = :repoDeletionMode")
    ,
    @NamedQuery(name = "HarRepo.findByRepoEmail", query = "SELECT h FROM HarRepo h WHERE h.repoEmail = :repoEmail")
    ,
    @NamedQuery(name = "HarRepo.findByRepoCompression", query = "SELECT h FROM HarRepo h WHERE h.repoCompression = :repoCompression")
    ,
    @NamedQuery(name = "HarRepo.findByRepoLink", query = "SELECT h FROM HarRepo h WHERE h.repoLink = :repoLink")
    ,
    @NamedQuery(name = "HarRepo.findByRepoLastSyncDate", query = "SELECT h FROM HarRepo h WHERE h.repoLastSyncDate = :repoLastSyncDate")
    ,
    @NamedQuery(name = "HarRepo.findByRepoRowUpdateTime", query = "SELECT h FROM HarRepo h WHERE h.repoRowUpdateTime = :repoRowUpdateTime")
    ,
    @NamedQuery(name = "HarRepo.findByRecordCount", query = "SELECT h FROM HarRepo h WHERE h.recordCount = :recordCount")

})
public class HarRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "repo_id", nullable = false)
    private Integer repoId;

    @Column(name = "repo_uid")
    @Size(min = 1, max = 200)
    @NotNull
    private String repoUID;

    @Basic(optional = false)
    @Size(min = 1, max = 500)
    @Column(name = "repo_name", length = 500)
    private String repoName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "repo_base_url", length = 255)
    private String repoBaseUrl;

    @Column(name = "repo_earliest_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date repoEarliestTimestamp;

    @Size(max = 255)
    @Column(name = "repo_granularity_date", length = 255)
    private String repoGranularityDate;

    @Size(max = 255)
    @Column(name = "repo_deletion_mode", length = 255)
    private String repoDeletionMode;

    @Size(max = 500)
    @Column(name = "repo_email", length = 500)
    private String repoEmail;
    @Lob
    @Size(max = 65535)
    @Column(name = "repo_desc", length = 65535)
    private String repoDesc;

    @Size(max = 500)
    @Column(name = "repo_compression", length = 500)
    private String repoCompression;

    @Size(max = 255)
    //temp commented
    @NotNull
    @Column(name = "repo_link", length = 255)
    private String repoLink;

    @Column(name = "repo_last_sync_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date repoLastSyncDate;

    @Column(name = "repo_row_update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date repoRowUpdateTime;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repoId")
    private Collection<HarMetadataTypeRepository> harMetadataTypeRepositoryCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repoId")
    private Collection<HarRecord> harRecordCollection;

    @JoinColumn(name = "repo_status_id", referencedColumnName = "repo_status_id", nullable = false)
    @ManyToOne(optional = false)
    private HarRepoStatus repoStatusId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repoId")
    private Collection<HarRepoMetadata> harRepoMetadataCollection;

    @Column(name = "record_count")
    private Long recordCount;

    @Column(name = "repo_last_sync_end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date repoLastSyncEndDate;

    @Column(name = "statusDescription")
    private String statusDescription;

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public HarRepo() {
    }

    public HarRepo(Integer repoId) {
        this.repoId = repoId;
    }

    public HarRepo(
            Integer repoId //            , String repoName
            ,
             String repoBaseUrl
    //            , Date repoRegistrationDate
    ) {
        this.repoId = repoId;
//        this.repoName = repoName;
        this.repoBaseUrl = repoBaseUrl;
//        this.repoRegistrationDate = repoRegistrationDate;
    }

    public Integer getRepoId() {
        return repoId;
    }

    public void setRepoId(Integer repoId) {
        this.repoId = repoId;
    }

    public String getRepoUID() {
        return repoUID;
    }

    public void setRepoUID(String repoUID) {
        this.repoUID = repoUID;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoBaseUrl() {
        return repoBaseUrl;
    }

    public void setRepoBaseUrl(String repoBaseUrl) {
        this.repoBaseUrl = repoBaseUrl;
    }

    public Date getRepoEarliestTimestamp() {
        return repoEarliestTimestamp;
    }

    public void setRepoEarliestTimestamp(Date repoEarliestTimestamp) {
        this.repoEarliestTimestamp = repoEarliestTimestamp;
    }

    public String getRepoGranularityDate() {
        return repoGranularityDate;
    }

    public void setRepoGranularityDate(String repoGranularityDate) {
        this.repoGranularityDate = repoGranularityDate;
    }

    public String getRepoDeletionMode() {
        return repoDeletionMode;
    }

    public void setRepoDeletionMode(String repoDeletionMode) {
        this.repoDeletionMode = repoDeletionMode;
    }

    public String getRepoEmail() {
        return repoEmail;
    }

    public void setRepoEmail(String repoEmail) {
        this.repoEmail = repoEmail;
    }

    public String getRepoDesc() {
        return repoDesc;
    }

    public void setRepoDesc(String repoDesc) {
        this.repoDesc = repoDesc;
    }

    public String getRepoCompression() {
        return repoCompression;
    }

    public void setRepoCompression(String repoCompression) {
        this.repoCompression = repoCompression;
    }

    public String getRepoLink() {
        return repoLink;
    }

    public void setRepoLink(String repoLink) {
        this.repoLink = repoLink;
    }

    public Date getRepoLastSyncDate() {
        return repoLastSyncDate;
    }

    public void setRepoLastSyncDate(Date repoLastSyncDate) {
        this.repoLastSyncDate = repoLastSyncDate;
    }

    public Date getRepoRowUpdateTime() {
        return repoRowUpdateTime;
    }

    public void setRepoRowUpdateTime(Date repoRowUpdateTime) {
        this.repoRowUpdateTime = repoRowUpdateTime;
    }

    @XmlTransient
    public Collection<HarMetadataTypeRepository> getHarMetadataTypeRepositoryCollection() {
        return harMetadataTypeRepositoryCollection;
    }

    public void setHarMetadataTypeRepositoryCollection(Collection<HarMetadataTypeRepository> harMetadataTypeRepositoryCollection) {
        this.harMetadataTypeRepositoryCollection = harMetadataTypeRepositoryCollection;
    }

    @XmlTransient
    public Collection<HarRecord> getHarRecordCollection() {
        return harRecordCollection;
    }

    public void setHarRecordCollection(Collection<HarRecord> harRecordCollection) {
        this.harRecordCollection = harRecordCollection;
    }

    public HarRepoStatus getRepoStatusId() {
        return repoStatusId;
    }

    public void setRepoStatusId(HarRepoStatus repoStatusId) {
        this.repoStatusId = repoStatusId;
    }

    public Long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Long recordCount) {
        this.recordCount = recordCount;
    }

    public Date getRepoLastSyncEndDate() {
        return repoLastSyncEndDate;
    }

    public void setRepoLastSyncEndDate(Date repoLastSyncEndDate) {
        this.repoLastSyncEndDate = repoLastSyncEndDate;
    }

    @XmlTransient
    public Collection<HarRepoMetadata> getHarRepoMetadataCollection() {
        return harRepoMetadataCollection;
    }

    public void setHarRepoMetadataCollection(Collection<HarRepoMetadata> harRepoMetadataCollection) {
        this.harRepoMetadataCollection = harRepoMetadataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repoId != null ? repoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HarRepo)) {
            return false;
        }
        HarRepo other = (HarRepo) object;
        if ((this.repoId == null && other.repoId != null) || (this.repoId != null && !this.repoId.equals(other.repoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "in.gov.nvli.harvester.beans.HarRepo[ repoId=" + repoId + " ]";
    }

    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
