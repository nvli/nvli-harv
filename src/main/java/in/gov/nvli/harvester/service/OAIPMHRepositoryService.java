/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.service;

import in.gov.nvli.harvester.beans.HarMetadataType;
import in.gov.nvli.harvester.beans.HarMetadataTypeRepository;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.controllers.SSLClientFactory;
import in.gov.nvli.harvester.custom.harvester_enum.HarRecordMetadataTypeEnum;
import in.gov.nvli.harvester.dao.HarMetadataTypeDao;
import in.gov.nvli.harvester.dao.HarMetadataTypeRepositoryDao;
import in.gov.nvli.harvester.oaipmhbeans.MetadataFormatType;
import in.gov.nvli.harvester.services.RepositoryService;
import in.gov.nvli.harvester.utilities.OAIBeanConverter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Service
public class OAIPMHRepositoryService {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private HarMetadataTypeDao harMetadataTypeDao;

    @Autowired
    private HarMetadataTypeRepositoryDao harMetadataTypeRepositoryDao;

    public String checkURLConnectivity(String url) {
        String urlStatus = "suceess";
        int responseCode;
        try {
            if (url.startsWith("https")) {
                // Create a context that doesn't check certificates.
                SSLContext sslCtx = SSLClientFactory.getSSlContext();
                TrustManager[] trustMgr = getTrustMgr();
                sslCtx.init(null, // key manager
                        trustMgr, // trust manager
                        new SecureRandom()); // random number generator
                HttpsURLConnection.setDefaultSSLSocketFactory(sslCtx.getSocketFactory());
                HttpsURLConnection con = (HttpsURLConnection) new URL(url).openConnection();
                con.setRequestMethod("HEAD");
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);
                responseCode = con.getResponseCode();

            } else {
                HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                con.setRequestMethod("HEAD");
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);
                try {
                    responseCode = con.getResponseCode();
                } catch (IOException e) {
                    return "Please check URL.";
                }
            }

            switch (responseCode) {
                case 200:
                    urlStatus = "suceess";
                    break;
                case 503:
                    urlStatus = "The server is currently unavailable. Could not get the information.";
                    break;
                default:
                    urlStatus = "Please check URL.";
                    break;
            }
        } catch (SocketTimeoutException st) {
            urlStatus = "Connection time out. Please check URL";

        } catch (UnknownHostException un) {
            urlStatus = "For this url host is unkown. Please check URL";

        } catch (MalformedURLException ex) {
            urlStatus = "URL is invalid. Please check URL";
        } catch (IOException ex) {
            urlStatus = "URL is invalid. Please check URL";
        } catch (KeyManagementException ex) {
            Logger.getLogger(OAIPMHRepositoryService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return urlStatus;
    }

    public TrustManager[] getTrustMgr() {
        return new TrustManager[]{new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
    }

    public boolean checkDuplicateURL(String baseUrl) {
        boolean isDuplicate = true;
        try {
            HarRepo checkUrl = repositoryService.getRepository(baseUrl);
            isDuplicate = (checkUrl == null || checkUrl.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
        } catch (Exception ex) {
            Logger.getLogger(OAIPMHRepositoryService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isDuplicate;
    }

    public boolean isSupportsMetadata(String metadata) {

        for (HarRecordMetadataTypeEnum c : HarRecordMetadataTypeEnum.values()) {
            if (c.name().equals(metadata)) {
                return true;
            }
        }
        return false;
    }

    public boolean saveOrUpdateRecursive(HarRepo harRepoObj, List<MetadataFormatType> metaDataFormats) {

        List<HarMetadataType> harMetadataTypeList = OAIBeanConverter.convertMetadataFormatTypeToHarMetadataType(metaDataFormats);
        if (harMetadataTypeDao.saveHarMetadataTypes(harMetadataTypeList)) {
            return saveHarMetadataTypeRepositoryList(harMetadataTypeList, harRepoObj);
        }
        return false;
    }

    public boolean saveHarMetadataTypeRepositoryList(List<HarMetadataType> harMetadataTypeList, HarRepo repository) {
        HarMetadataTypeRepository obj = null;
        List<HarMetadataTypeRepository> metadatOfRepo = new ArrayList<>();
        for (HarMetadataType meataData : harMetadataTypeList) {
            obj = new HarMetadataTypeRepository();
            obj.setRepoId(repository);
            obj.setMetadataTypeId(harMetadataTypeDao.getMetadataTypeByMetadatPrefix(meataData.getMetadataPrefix()));
            metadatOfRepo.add(obj);
        }
        return harMetadataTypeRepositoryDao.saveHarMetadataTypesOfRepository(metadatOfRepo);
    }
}
