/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.service;

import in.gov.nvli.harvester.oaipmhbeans.MetadataFormatType;
import in.gov.nvli.harvester.oaipmhbeans.VerbType;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.beans.HarRepoMetadata;
import in.gov.nvli.harvester.constants.CommonConstants;
import in.gov.nvli.harvester.constants.HarvesterLogConstants;
import in.gov.nvli.harvester.controllers.GetRecordController;
import in.gov.nvli.harvester.custom.exception.OAIPMHerrorTypeException;
import in.gov.nvli.harvester.custom.harvester_enum.HarRecordMetadataTypeEnum;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import in.gov.nvli.harvester.customised.IdentifyTypeCustomised;
import in.gov.nvli.harvester.dao.HarRepoMetadataDao;
import in.gov.nvli.harvester.services.IdentifyService;
import in.gov.nvli.harvester.services.ListMetadataFormatsService;
import in.gov.nvli.harvester.services.ListRecordsService;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Service
public class RepositoryHarvestService {

    @Autowired
    public IdentifyService identifyService;

    @Autowired
    public ListMetadataFormatsService listMetadataFormatsService;

    @Autowired
    private HarRepoMetadataDao harRepoMetadataDaoObj;

    @Autowired
    private ListRecordsService listRecordsService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GetRecordController.class);
    public boolean error = false;
    public boolean dataSaved = false;

    public void harvestRepositoryByRecordMetadataType(HarRepo harRepo, HarRecordMetadataTypeEnum metadataType, Boolean harFlag) {
        HarRepoMetadata harRepoMetadataObj;
        System.out.println("");
        harRepoMetadataObj = harRepoMetadataDaoObj.get(harRepo.getRepoId(), metadataType);
        Short harRepoStatusId = harRepoMetadataObj.getHarvestStatus().getRepoStatusId();
        if (harRepoStatusId == 2 || harRepoStatusId == 4 || harRepoStatusId == 7) {
            if (harRepoMetadataObj.getEnableFlag() == CommonConstants.ENABLED) {
                if (saveRecordsByMetadataType(metadataType, harRepoMetadataObj, harFlag)) {
                    LOGGER.info(harRepo.getRepoUID() + ":" + HarvesterLogConstants.LISTRECORDS_SAVED + " : " + metadataType.value());
                    if (metadataType.value() == "ore") {
                        dataSaved = true;
                    }
                } else {
                    LOGGER.error(harRepo.getRepoUID() + ":" + VerbType.LIST_RECORDS.value() + " : " + metadataType.value() + " is not saved");
                    harRepoMetadataDaoObj.updateStatusDiscription(harRepoMetadataObj, "Uanble to save " + metadataType.value() + " list records");
                    error = true;
                }
            }
        }
    }

    public Boolean saveRecordsByMetadataType(HarRecordMetadataTypeEnum metadataType, HarRepoMetadata harRepoMetadataObj, Boolean harFlag) {
        switch (metadataType) {
            case OAI_DC:
                if (harFlag) {
                    try {
                        return listRecordsService.saveOrUpdateListRecords(harRepoMetadataObj, MethodEnum.GET, "");
                    } catch (OAIPMHerrorTypeException | ParseException | JAXBException | IOException ex) {
                        Logger.getLogger(RepositoryHarvestService.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                } else {
                    return listRecordsService.saveListRecords(harRepoMetadataObj, MethodEnum.GET, "");
                }
            case ORE:
                if (harFlag) {
                    return listRecordsService.saveOrUpdateListRecordsXML(harRepoMetadataObj, MethodEnum.GET, "", true);
                } else {
                    return listRecordsService.saveListRecordsXML(harRepoMetadataObj, MethodEnum.GET, "", true);
                }
            case METS:
            case MARC:
            case MARCXML:
                if (harFlag) {
                    return listRecordsService.saveOrUpdateListRecordsXML(harRepoMetadataObj, MethodEnum.GET, "", !dataSaved);
                } else {
                    return listRecordsService.saveListRecordsXML(harRepoMetadataObj, MethodEnum.GET, "", !dataSaved);
                }

            default:
                return false;
        }
    }
}
