/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.custom.harvester_enum;

/**
 *
 * @author ankit
 */
public enum HarRecordMetadataTypeEnum {
    OAI_DC("oai_dc"),
    ORE("ore"),
    METS("mets"),
    MARC("marc"),
    MARCXML("marcxml");

    private final String text;


    private HarRecordMetadataTypeEnum(final String text) {
        this.text = text;
    }

    public String value() {
        return this.text;
    }

    public static boolean contains(HarRecordMetadataTypeEnum e) {
        for (HarRecordMetadataTypeEnum value : HarRecordMetadataTypeEnum.values()) {
            if (value == e) {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(String e) {
        for (HarRecordMetadataTypeEnum value : HarRecordMetadataTypeEnum.values()) {
            if (value.text.equals(e)) {
                return true;
            }
        }
        return false;
    }

    public static HarRecordMetadataTypeEnum getBasedOnText(String e) {

        for (HarRecordMetadataTypeEnum value : HarRecordMetadataTypeEnum.values()) {
            if (value.text.equals(e)) {
                return value;
            }
        }
        return null;
    }


}
