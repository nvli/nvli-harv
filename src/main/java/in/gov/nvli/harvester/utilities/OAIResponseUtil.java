/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vootla
 */
public class OAIResponseUtil {

    public static String createResponseFromXML(HttpURLConnection con) {
        StringBuilder response = new StringBuilder();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        } catch (IOException ex) {
            Logger.getLogger(OAIResponseUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(OAIResponseUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return response.toString();
    }

    public static String createResponseFromXML(File file) {
        StringBuilder response = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OAIResponseUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OAIResponseUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return response.toString();
    }
}
