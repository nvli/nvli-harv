/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.listener.hibernate;

import in.gov.nvli.harvester.services.DataImportService;
import javax.annotation.PostConstruct;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Component
public class ApplicationListenerImpl implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DataImportService dataImportService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        //implement abstract method
    }

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ApplicationListenerImpl.class);

    @PostConstruct
    public void inIt() {
        try {
            dataImportService.harRepoStatusDataImportInDB();
        } catch (Exception ex) {
            LOGGER.error("ApplicationListenerImpl inIt method Error while running start scripts", ex);
        }
    }

}
