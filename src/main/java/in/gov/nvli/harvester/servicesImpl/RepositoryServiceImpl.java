/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.servicesImpl;

import in.gov.nvli.harvester.beans.HarMetadataTypeRepository;
import in.gov.nvli.harvester.beans.HarRepo;
import in.gov.nvli.harvester.beans.HarRepoMetadata;
import in.gov.nvli.harvester.custom.exception.OAIPMHerrorTypeException;
import in.gov.nvli.harvester.custom.harvester_enum.MethodEnum;
import in.gov.nvli.harvester.dao.HarMetadataTypeRepositoryDao;
import in.gov.nvli.harvester.dao.RepositoryDao;
import in.gov.nvli.harvester.services.ListMetadataFormatsService;
import in.gov.nvli.harvester.services.RepositoryMetadataService;
import in.gov.nvli.harvester.services.RepositoryService;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.hibernate.Hibernate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ankit
 */
@Service
public class RepositoryServiceImpl implements RepositoryService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RepositoryServiceImpl.class);

    @Autowired
    private RepositoryDao repositoryDaoObject;

    @Autowired
    private HarMetadataTypeRepositoryDao harMetadataTypeRepositoryDaoObj;

    @Autowired
    private ListMetadataFormatsService listMetadataFormatsServiceObj;

    @Autowired
    private RepositoryMetadataService repositoryMetadataService;

    @Override
    public HarRepo addRepository(HarRepo repositoryObject) {
        return repositoryDaoObject.addRepository(repositoryObject);

    }

    @Override
    public List<HarRepo> getAllRepositories() {
        return repositoryDaoObject.list();
    }

    @Override
    public HarRepo getRepository(int repositoryId) {
        return repositoryDaoObject.get(repositoryId);
    }

    @Override
    public List<HarRepo> getRepositoriesByStatus(List<Integer> statusIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void editRepository(HarRepo repositoryObject) {
        LOGGER.info("EDITING HarRepo " + repositoryObject.getRepoBaseUrl());
        repositoryDaoObject.merge(repositoryObject);
    }

    @Override
    public boolean deleteRepository(int repositoryId) {
        repositoryDaoObject.delete(repositoryDaoObject.get(repositoryId));
        HarRepo r = repositoryDaoObject.get(repositoryId);
        if (r == null) {
            LOGGER.info("Deleted HarRepo  " + repositoryId);
            return true;
        } else {
            LOGGER.info("Unable to delete HarRepo of ID " + repositoryId);
            return false;
        }

    }

    @Override
    public void deleteRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void activateOrDeactivateRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void activateOrDeactivateRepository(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void publishOrWithdrawRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void publishOrWithdrawRepository(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void blockRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void blockRepository(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void synchroniseRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void synchroniseRepository(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void scheduleSynchronisationOfRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void scheduleSynchronisationOfRepository(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validateRepositories(List<Integer> repositoryIds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isRepositoryValid(int repositoryId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HarRepo getRepositoryByUID(String repoUID) {
        return repositoryDaoObject.getRepositoryByUID(repoUID);
    }

    @Override
    public List<HarRepo> getRepositoriesByUIDS(List<String> repoUIDS) {
        return repositoryDaoObject.getRepositories(repoUIDS);
    }

    @Override
    public boolean changeRepoStatus(List<String> repositoryUIDs, short status) {
        return repositoryDaoObject.changeRepoStatus(repositoryUIDs, status);
    }

    @Override
    public boolean changeRepoStatus(String repositoryUID, short status) {
        return repositoryDaoObject.changeRepoStatus(repositoryUID, status);
    }

    @Override
    public boolean changeRepoStatus(short status) {
        List<HarRepo> repos = repositoryDaoObject.getRepositories();
        return repositoryDaoObject.changeRepoStatusByHarRepo(repos, status);
    }

    @Override
    public List<HarRepo> getActiveRepositories() {
        return repositoryDaoObject.getActiveRepositories();
    }

    @Override
    public List<HarRepo> getRepositoriesByStaus(short repoStatusId) {
        return repositoryDaoObject.getRepositoriesByStaus(repoStatusId);

    }

    @Override
    public List<HarMetadataTypeRepository> list(HarRepo harRepoObj) {
        return harMetadataTypeRepositoryDaoObj.list(harRepoObj);
    }

    @Override
    public void saveHarMetadataTypes(HarRepo harRepoObj) {
        try {
            listMetadataFormatsServiceObj.saveHarMetadataTypes(harRepoObj.getRepoBaseUrl(), MethodEnum.GET, "");
        } catch (IOException | JAXBException | OAIPMHerrorTypeException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public JSONObject getRepoDetails(String repo_uid) {
        JSONObject object = new JSONObject();
        HarRepo harRepo = this.getRepositoryByUID(repo_uid);
        object.put("repoId", harRepo.getRepoId());
        object.put("repoName", harRepo.getRepoName());
        object.put("repoDesc", harRepo.getRepoDesc());
        object.put("repoUID", harRepo.getRepoUID());
        object.put("repoBaseURL", harRepo.getRepoBaseUrl());
        object.put("repoStatus", harRepo.getRepoStatusId().getRepoStatusName());
        List<HarRepoMetadata> repoMetadatas = repositoryMetadataService.list(harRepo);
        JSONArray metadataArray = new JSONArray();
        for (HarRepoMetadata repoMetadata : repoMetadatas) {
            JSONObject o = new JSONObject();
            o.put("metadataType", repoMetadata.getMetadataTypeId().getMetadataPrefix());
            o.put("status", repoMetadata.getHarvestStatus().getRepoStatusName());
            o.put("statusDesc", repoMetadata.getHarvestStatus().getRepoStatusDesc());
            o.put("metadataId", repoMetadata.getRepoMetadataId());
            o.put("nextAction", repoMetadata.getHarvestStatus().getNextAction());
            metadataArray.add(o);
        }
        object.put("metadataList", metadataArray);
        return object;
    }

    @Override
    public HarRepo getRepository(String baseUrl) {
        return repositoryDaoObject.getRepository(baseUrl);

    }

}
