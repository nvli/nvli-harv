/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.dao;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
public interface DataImportDao {

    public boolean harRepoStatusDataImportInDB();
}
