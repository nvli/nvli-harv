/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.dao;

import in.gov.nvli.harvester.beans.HarRepoStatus;

/**
 *
 * @author ankit
 */
public interface HarRepoStatusDao extends GenericDao<HarRepoStatus, Short> {

}
