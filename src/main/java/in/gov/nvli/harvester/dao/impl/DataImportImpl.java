/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.harvester.dao.impl;

import in.gov.nvli.harvester.dao.DataImportDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Repository
public class DataImportImpl implements DataImportDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean harRepoStatusDataImportInDB() {
        try {
            DataSource dataSource = SessionFactoryUtils.getDataSource(sessionFactory);
            ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
            rdp.addScript(new ClassPathResource("create_har_repo_status.sql"));
            rdp.setContinueOnError(true);
            rdp.setSqlScriptEncoding("utf8");
            Connection connection = dataSource.getConnection();
            rdp.populate(connection); // this starts the script execution, in the order as added
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataImportImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
