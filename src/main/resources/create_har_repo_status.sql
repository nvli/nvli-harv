/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Ankita Dhongde <dankita@cdac.in>
 * Created: 26 Sep, 2017
 */
-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 26, 2017 at 11:36 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `harvester_18_08_2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `har_repo_status`
--

CREATE TABLE `har_repo_status` (
  `repo_status_id` smallint(6) NOT NULL,
  `repo_status_desc` longtext,
  `repo_status_name` varchar(255) NOT NULL,
  `next_action` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `har_repo_status`
--

INSERT INTO `har_repo_status` (`repo_status_id`, `repo_status_desc`, `repo_status_name`, `next_action`) VALUES
(1, 'Repository Not Activated', 'not_active', 'Activate'),
(2, 'Repository is activated for harvesting', 'active', 'Start Harvesting'),
(3, 'Harvest operation is in process', 'harvest_processing', 'show_progress'),
(4, 'Some error occurred while processing harvest operations', 'harvest_processing_error', 'Start Incremental Harvest'),
(5, 'Hartvesting is successfully completed', 'harvest_complete', 'Start Incremental Harvest'),
(6, 'Incremental Harvesting is in progress', 'increment_harvest_processing', 'show_progress'),
(7, 'Some error occurred while processing incremental harvest operations', 'increment_harvest_processing_error', 'Start Incremental Harvest'),
(8, 'Invalid Repository Base URL', 'invalid_url', 'Check URL'),
(9, 'Some error occurred while save repository and start harvesting', 'unable_to_process', 'Start Harvesting');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `har_repo_status`
--
ALTER TABLE `har_repo_status`
  ADD PRIMARY KEY (`repo_status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `har_repo_status`
--
ALTER TABLE `har_repo_status`
  MODIFY `repo_status_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

